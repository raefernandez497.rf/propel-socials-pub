<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.landing');
})->name('landing');

Route::get('/services', function () {
    return view('front.services');
})->name('services');

Route::get('/about', function () {
    return view('front.about');
})->name('about');

Route::get('/our-work', function () {
    return view('front.our-work');
})->name('our-work');

Route::get('/plans-and-pricing', function () {
    return view('front.pricing');
})->name('pricing');

Route::get('/dashboard', 'HomeController@showDashboard')
->name('showDashboard');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile', 'HomeController@profile')->name('profile');

Route::get('/work-with-us', function () {
    return view('front.work-with');
})->name('work-with');

Route::post('/checkdata', function () {
    dd(\Request::all());
})->name('checkData');

/** Inbox */
Route::group(['prefix' => 'inbox'], function () {
    Route::get('/', 'AdminController@showInbox')
        ->name('showAdminInbox');

    Route::get('/new-message', function(){
        return redirect()->route('showClients')->with('message', 'message');
    })->name('newAdminMessage');

    Route::get('/details/{id}', 'AdminController@inboxDetails')
        ->name('inboxDetails');

    Route::get('/composition/{id}', 'AdminController@newMessage')
        ->name('newComposition');

    Route::post('/composition/save', 'AdminController@saveSentMessage')
        ->name('saveComposition');
});

/** Admin */
Route::group(['prefix' => 'admin'], function () {

    Route::get('/dashboard', function () {
        return view('inside.admin.dash');
    })->name('admindash');

    Route::post('/updateuser', 'AdminController@updateAdmin')->name('updateAdmin');

    Route::get('/profile', 'HomeController@showProfile')->name('adminprofile');

    /** Messages */
    Route::get('/messages', 'AdminController@showMessages')
        ->name('showMessages');

    Route::get('/messages/{id}', 'AdminController@showMessageDetails')
        ->name('showMDetails');


    Route::get('/clients', 'ClientsController@showClients')
        ->name('showClients');

    Route::post('/send-message', 'AdminController@sendMessage')
        ->name('sendMessage');
    /** Users */
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'HomeController@showUsers')
        ->name('showUsers');

        Route::get('/add', 'AdminController@showAddUser')
        ->name('showAddUser');

        Route::post('/save', 'AdminController@saveUser')
        ->name('saveUser');

        Route::get('/delete/{id}', 'AdminController@deleteUser')
        ->name('deleteUser');
    });
    /** Packages */
    Route::group(['prefix' => 'packages'], function () {
        Route::get('/', 'AdminController@showPackages')
        ->name('showPackages');

        Route::post('/renew', 'AdminController@renewSub')
        ->name('renewSub');

        Route::get('/details/{id}', 'AdminController@showPDetails')
        ->name('showPDetails');

        Route::get('/new/subscription', 'AdminController@renewSubRedirect')
        ->name('renewRedirect');
    });
    Route::post('/savereport', 'ClientsController@saveReport')
        ->name('saveReport');

    Route::get('/reports/{id}', 'ClientsController@showReports')
        ->name('showReports');
        
    /** Client management */
    Route::group(['prefix' => 'client'], function () {
        
        Route::get('/details/{id}', 'ClientsController@showDetails')
                ->name('showDetails');
        
        // Route::get('/reports/{id}', 'ClientsController@showClientReport')
        //         ->name('showClientReports');

        Route::post('/profile/save', 'ClientsController@saveProfile')
            ->name('saveClientProfile');
    });
});

// Route::get('/landingpage', function () {
//     return view('layouts.landingpage');
// });

Route::group(['prefix' => 'client'], function () {
    Route::get('/dashboard', function () {
        return view('inside.client.dash');
    })->name('clientdash');

    Route::get('/profile', 'HomeController@showProfile')
        ->name('clientprofile');

    Route::get('/report', 'ClientsController@showClientReport')
        ->name('clientreports');

    Route::get('/messages', 'ClientsController@showInbox')
        ->name('showClientMessages');
});
