<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacebookModel extends Model
{
    /** Initialize table reference */
    protected $table = 'facebook';

    /** Initialize custom primary key */
    protected $primaryKey = 'fb_id';

}
