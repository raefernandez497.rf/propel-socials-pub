<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    /** Initialize table reference */
    protected $table = 'user_details';

    /** Relationship towards Users table */
    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /** Relationship towards Business table*/
    public function business()
    {
        return $this->belongsTo('App\Business', 'business_id');
    }

    /** Relationship with table `Facebook` */
    public function fb()
    {
        return $this->belongsTo('App\FacebookModel', 'fb_id');
    }

    /** Relationship with table `Twitter` */
    public function twitter()
    {
        return $this->belongsTo('App\TwitterModel', 'twitter_id');
    }

    /** Relationship with table `Instagram` */
    public function insta()
    {
        return $this->belongsTo('App\InstaModel', 'ints_id');
    }
}
