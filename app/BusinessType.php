<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessType extends Model
{
    /** Initialize table reference */
    protected $table = 'business_types';

    /** Initialize custom primary key */
    protected $primaryKey = 'type_id';
}
