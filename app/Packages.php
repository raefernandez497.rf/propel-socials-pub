<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    /** Initialize table referece */
    protected $table = 'packages';

    /** Initialize custom primary key */
    protected $primaryKey = 'package_id';

    /** Initialize relationship with `Package_details` */
    public function details()
    {
        return $this->belongsTo('App\PackageDetails');
    }
}
