<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryModel extends Model
{
    
    protected $table = 'report_history';

    protected $primaryKey = 'history_id';

    
    public function fb()
    {
        return $this->belongsTo('App\FbHistory');
    }

    public function twitter()
    {
        return $this->belongsTo('App\TwitterHistory');
    }

    public function insta()
    {
        return $this->belongsTo('App\InstaHistory');
    }
}
