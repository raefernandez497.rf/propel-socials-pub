<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /** Show dashboard */
    public function showDashboard()
    {
        /** Check if user is 
         * 1 = Admin or 
         * 2 = Client
         */
        if(((auth()->user()->type_id)==1) || ((auth()->user()->type_id)==2))
        {
            return redirect()->route('admindash');
        }
        else
        {
            // $uid = auth()->user()->id;

            // $detalis = \App\UserDetails::where('user_id', '=', $uid)->get();

            // $subs = \App\PackageMgt::where('user_id', '=', $uid)->orderby('mgt_id', 'desc')->limit(1)->get();

            return redirect()->route('clientdash');            
        }
    }

    /** Show user Profile */
    public function profile()
    {
        if(((auth()->user()->type_id)==1) || ((auth()->user()->type_id)==2))
        {
            return redirect()->route('adminprofile');
        }
        else {
            return redirect()->route('clientprofile');
        }
    }

    /** Show user list */
    public function showUsers()
    {
        if(((auth()->user()->type_id)==1) || ((auth()->user()->type_id)==2))
        {
            $users = User::paginate(5);

            return view('inside.admin.users.users', compact('users'));
        }
        else {
            return view('layouts.errorpage');
        }
    }

    /** Show user profile */
    public function showProfile()
    {
        if((auth()->user()->type_id==1) || (auth()->user()->type_id==2))
        {
            $users = User::where('id','=',auth()->user()->id)->get();
            return view('inside.admin.profile', compact('users'));
        }
        else
        {
            $details = \App\UserDetails::where('user_id','=',auth()->user()->id)->get();
            return view('inside.client.profile', compact('details'));    
        }
    }
}
