<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use App\UserDetails;
use App\Business;
use App\FacebookModel;
use App\TwitterModel;
use App\InstaModel;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255'],
            'mname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'sex' => ['required'],
            'birthday' => ['required'],
            'contactno' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            
        ]);
        // 'g-recaptcha-response' => ['required']
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        /** Get the next user id */
        $statement = DB::select("SHOW TABLE STATUS LIKE 'business'");
        /** Assign the next user id to $nextBId */
        $nextBId = $statement[0]->Auto_increment;

        /** Get the next User id */
        $statement = DB::select("SHOW TABLE STATUS LIKE 'users'");
        /** Assign the retrieved id to a variable named `$nextUId` */
        $nextUId = $statement[0]->Auto_increment;

        /** Check first if user has entered social media accounts 
         *  before getting the id
        */
        if ($data['fb_name']!='') {
            /** Get the next Fb ID*/
            $statement = DB::select("SHOW TABLE STATUS LIKE 'facebook'");
            /** Assign the retrieved id to a variable named `$nextFId` */
            $nextFId = $statement[0]->Auto_increment;

            $fb = new FacebookModel;
            $fb->fb_name = $data['fb_name'];
            $fb->fb_url = $data['fb_url'];
            // $fb->touch();
            $fb->save();
        }
        
        if ($data['twitter_name']!='') {
            /** Get the next Twitter ID*/
            $statement = DB::select("SHOW TABLE STATUS LIKE 'twitter'");
            /** Assign the retrieved id to a variable named `$nextTId` */
            $nextTId = $statement[0]->Auto_increment;

            $twit = new TwitterModel;
            $twit->twitter_name = $data['twitter_name'];
            $twit->twitter_url = $data['twitter_url'];
            // $twit->touch();
            $twit->save();
        }
        
        if ($data['inst_name']!='') {
            /** Get the next Instagram ID */
            $statement = DB::select("SHOW TABLE STATUS LIKE 'instagram'");
            /** Assign the retrieved id to a variable named `$nextIId` */
            $nextIId = $statement[0]->Auto_increment;

            $inst = new InstaModel;
            $inst->inst_name = $data['inst_name'];
            $inst->inst_url = $data['inst_url'];
            // $inst->touch();
            $inst->save();
        }

        /** Use Carbon to count 30 days for the activation of the package */

        $mgt = new \App\PackageMgt;
        $mgt->user_id = $nextUId;
        $mgt->datestart = Carbon::now();
        $mgt->dateend = Carbon::now()->addDays(30);
        $mgt->isCancelled = 0;
        $mgt->package_id = $data['plan'];
        // $mgt->touch();
        $mgt->save();

        $business = new Business;
        $business->business_name = $data['business_name'];
        $business->business_type = $data['business_type'];
        $business->business_address = $data['business_address'];
        $business->date_established = $data['date_established'];
        // $business->touch();
        $business->save();

        $userdetails = new UserDetails;
        $userdetails->card_info = $data['card_info_1'] . '-' . $data['card_info_2'] . '-' . $data['card_info_3'];
        $userdetails->business_id = $nextBId;
        $userdetails->user_id = $nextUId;
        if (isset($nextFId)) {
            $userdetails->fb_id =$nextFId;
        }
        if (isset($nextTId)) {
            $userdetails->twitter_id =$nextTId;
        }
        if (isset($nextIId)) {
            $userdetails->inst_id =$nextIId;
        }
        // $userdetails->touch();
        $userdetails->save();

        return User::create([
            'fname' => $data['fname'],
            'mname' => $data['mname'],
            'lname' => $data['lname'],
            'sex' => $data['sex'],
            'birthday' => $data['birthday'],
            'contactNo' => $data['contactno'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'type_id' => 3,
        ]);
    }

}
