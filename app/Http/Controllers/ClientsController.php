<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserDetails;
use App\PackageMgt;
use App\FacebookModel;
use App\TwitterModel;
use App\InstaModel;

class ClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /** Show Client list */
    public function showClients()
    {
        $details= UserDetails::paginate(5);

        return view('inside.admin.clients.clients', compact('details'));
    }

    /** Show Client details */
    public function showDetails($id)
    {
        $details = UserDetails::where('user_id','=',$id)->get();
        $mgts = PackageMgt::where('user_id','=',$id)->latest()->get();
        $subhist = PackageMgt::where('user_id','=',$id)->paginate(5);
        return view('inside.client.details', compact('details', 'id', 'mgts', 'subhist'));
    }

    /** Show Report Page for adding reports */
    public function showReports($id)
    {
        /** Check first if client has an active subscription */
        $sub = PackageMgt::where('user_id', '=', $id)
                            ->where('isCancelled','=',0)
                            ->count();

        $pid = PackageMgt::where('user_id', '=', $id)->latest()->pluck('package_id');

        /** get userid for the `user_details` */

        $pname = \App\Packages::find($pid)->pluck('name');

        $xpiry = PackageMgt::where('user_id', '=', $id)->latest()->pluck('dateend');

        $details = UserDetails::where('user_id','=',$id)->get();

        if ($sub==0) {
            return view('inside.admin.reports', compact('details','sub', 'pname', 'xpiry'))->with('error', 'error');
        }
        else {
            return view('inside.admin.reports', compact('details','sub', 'pname', 'xpiry'));
        }
    }

    /** Save reports */
    public function saveReport(Request $request)
    {
        /** Save the facebook, twitter, instagram report */
        /** Get mgt_id in table `Package_mgt`
         * so that it can be added to tables: fb_history, twitter_history
        */

        $data = $request->all();
        // dd($data);
        $uid = $data['userid'];
        $mgt_id = PackageMgt::where('user_id', '=', $uid)
                            ->where('isCancelled', '=', 0)
                            ->orderBy('mgt_id','desc')
                            ->limit(1)
                            ->pluck('mgt_id');

                            /** Get the next Fb ID*/
        $statement = DB::select("SHOW TABLE STATUS LIKE 'fb_history'");
        /** Assign the retrieved id to a variable named `$nextFId` */
        $nextFId = $statement[0]->Auto_increment;

        /** Get the next Twitter ID*/
        $statement = DB::select("SHOW TABLE STATUS LIKE 'twitter_history'");
        /** Assign the retrieved id to a variable named `$nextTId` */
        $nextTId = $statement[0]->Auto_increment;
        
        /** Get the next Instagram ID */
        $statement = DB::select("SHOW TABLE STATUS LIKE 'insta_history'");
        /** Assign the retrieved id to a variable named `$nextIId` */
        $nextIId = $statement[0]->Auto_increment;
        
        /** Before adding to tables, 
         * check first if the user
         * has the social media
         */

         if (isset($data['fdatestart'])) {
            $fb = new \App\FbHistory;
            $fb->datestart = $data['fdatestart'];
            $fb->dateend = $data['fdateend'];
            $fb->reach = $data['reach'];
            $fb->engagement = $data['fengagement'];
            $fb->shares = $data['shares'];
            $fb->ratings = $data['ratings'];
            $fb->mgt_id = $mgt_id[0];
            // $fb->touch();
            $fb->save();
         }
        
         if (isset($data['tdatestart'])) {
             $twit = new \App\TwitterHistory;
             $twit->datestart = $data['tdatestart'];
             $twit->dateend = $data['tdateend'];
             $twit->engagement = $data['tengagement'];
             $twit->impressions = $data['impressions'];
             $twit->mentions = $data['mentions'];
             $twit->followers = $data['followers'];
             $twit->engagement_rating = $data['eng_rating'];
             $twit->mgt_id = $mgt_id[0];
             // $twit->touch();
             $twit->save();
         }
        

        /** Instgram 
         * idatestart, idateend
        */
        if (isset($data['idatestart'])) {
            $inst = new \App\insta_history;
            $inst->datestart = $data['idatestart'];
            $inst->dateend = $data['idateend'];
            $inst->followers = $data['ifollowers'];
            $inst->mentions = $data['imentions'];
            $inst->reach = $data['ireach'];
            $inst->engagement = $data['iengagement'];
            $inst->impressions = $data['iimpressions'];
            $inst->mgt_id = $mgt_id[0];
            $inst->save();
        }

        $rhistory = new \App\HistoryModel;
        $rhistory->mgt_id = $mgt_id[0];
        $rhistory->fb_history_id = $nextFId;
        $rhistory->twitter_history_id = $nextTId;
        $rhistory->insta_history_id = $nextIId;
        $rhistory->message = $data['message'];
        $rhistory->save();

        return redirect()->route('showReports', ['id' => $data['userid']])->with('success','report successfully saved');
    }

    /** Show reports to client */
    public function showClientReport()
    {
        /** Get the latest package_mgt which is not cancelled
         * based on user_id
         * Query fb_history, twitter_history if it contains `mgt_id`
         * if none, return none
         */
        $id = auth()->user()->id;

        $mgt_id = PackageMgt::where('user_id','=',$id)->pluck('mgt_id');

        
        $rhistory = \App\HistoryModel::where('mgt_id', '=', $mgt_id[0])
                                        ->latest()
                                        ->get();

        $mgtdetails = PackageMgt::where('user_id', '=', $id)
                                        ->where('isCancelled', '=', 0)
                                        ->orderBy('mgt_id', 'desc')
                                        ->limit(1)
                                        ->get();
        if ($rhistory->count()==0) {
            $facebook = [];
            $twitter = [];
            $instagram = [];
            $udetails =[];
            return view('inside.client.reports', compact('facebook','twitter','instagram', 'mgtdetails', 'udetails', 'rhistory'))->with('noreport','no reports');
        }
        elseif($rhistory->count()!=0) {
            foreach ($rhistory as $item) {
                $info = [
                    'fbid' => $item->fb_history_id,
                    'twid' => $item->twitter_history_id,
                    'iid' => $item->insta_history_id,
                    'msg' => $item->message,
                ];
            }
    
            $message = $info['msg'];
            $facebook = \App\FbHistory::where('id','=', $info['fbid'])->latest()->get();
    
            $twitter = \App\TwitterHistory::where('id','=', $info['twid'])->latest()->get();
    
            $instagram = \App\insta_history::where('id','=', $info['iid'])->latest()->get();
    
            $udetails = UserDetails::where('user_id', '=', $id)
                                    ->get();
    
            // dd($rhistory);
            return view('inside.client.reports', compact('facebook','twitter','instagram', 'mgtdetails', 'udetails', 'message', 'rhistory'));
        }
    }

    /** Update client profile */
    public function saveProfile(Request $request)
    {
        $userid = auth()->user()->id;
        $data = $request->all();

        // dd($data);
        $user = User::find($userid);
        $user->fname = $data['fname'];
        $user->mname = $data['mname'];
        $user->mname = $data['lname'];
        $user->save();

        $detailid = \App\UserDetails::where('user_id','=',$userid)->pluck('detail_id');

        $detail = \App\UserDetails::find($detailid[0]);
        
        $detail->save();

        return redirect()->route('profile')->with('success','update success');
    }

    /** Show client inbox */
    public function showInbox()
    {
         /** Query table `messenger`
          * where `from` = `auth()->user()->id`
          */
          $userid = auth()->user()->id;

          /** Sent messages */
          $Smessages = \App\MessengerModel::where('from','=', $userid)
                      ->paginate(5);
  
          /** Received messages */
          $Rmessages = \App\MessengerModel::where('to','=', $userid)
                      ->paginate(5);
  
           return view('inside.client.inbox.showinbox', compact('Smessages', 'Rmessages'));
    }
}
