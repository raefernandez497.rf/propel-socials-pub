<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packages;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /** Show package page */
    public function showPackages()
    {
        if (((auth()->user()->type_id)==1) || ((auth()->user()->type_id)==2)) {

            $packages = Packages::paginate(5);
            return view('inside.admin.packages.packages', compact('packages'));
        
        }
        else
        {
            return view('layouts.errorpage');
        }
    }

    /** Show Add user form */
    public function showAddUser()
    {
        return view('inside.admin.users.adduser');
    }

    /** Save user to table `users` */
    public function saveUser(Request $request)
    {
        $data = $request->all();
        
        $user = new \App\User;
        $user->fname = $data['fname'];
        $user->mname = $data['mname'];
        $user->lname = $data['lname'];
        $user->sex = $data['sex'];
        $user->birthday = $data['birthday'];
        $user->contactNo = $data['contactno'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->type_id = 2;
        $user->save();

        return redirect()->route('showAddUser')->with('success', 'save success');
    }
    /** Send message to email */
    public function sendMessage(Request $request)
    {
        $data = $request->all();
        /** Save to table `messages` */
        $m = new \App\MessagesModel;
        $m->name = $data['fullname'];
        $m->email = $data['email'];
        $m->contact_no = $data['contact'];
        $m->message = $data['msg'];
        $m->save();

        return redirect()->route('landing')->with('sent','success sent');
    }

    /** Show list of messages */
    public function showMessages()
    {
        $messages = \App\MessagesModel::paginate(5);

        return view('inside.admin.messages', compact('messages'));
    }

    /** Show message details */
    public function showMessageDetails($id)
    {
        $messages = \App\MessagesModel::where('id', '=', $id)
                                    ->get();
        return view('inside.admin.m_details', compact('messages'));
    }

    /** Redirect to Renew Package Subscription Page */
    public function renewSubRedirect()
    {
        $packages = \App\Packages::get();

        return view('inside.admin.clients.renewsub', compact('packages'));
    }

    /** Update Admin Profile */
    public function updateAdmin(Request $request)
    {

        $data = $request->all();

        $oldpw = \App\User::where('id','=',$data['userid'])->pluck('password');

        $user = \App\User::find($data['userid']);
        $user->fname = $data['fname'];
        $user->mname = $data['mname'];
        $user->lname = $data['lname'];
        $user->contactNo = $data['contactno'];
        $user->email = $data['email'];
        if ((Hash::make($data['oldpass'])==$oldpw)) {
            $user->password = $data['newpass'];
        }
        $user->save();

        return redirect()->route('profile')->with('success','update success');
    }

    /** Remove user */
    public function deleteUser($id)
    {
        $ui = \App\User::find($id);
        $ui->delete();

        return redirect()->route('showUsers')->with('deleted', 'Task was successful!');
    }

    /** Show package details */
    public function showPDetails($id)
    {
        $packages = \App\PackageDetails::where('package_id', '=', $id)->get();

        return view('inside.admin.packages.package_details', compact('packages'));
        // dd($packages);
    }

    /** Renew subscription */
    public function renewSub(Request $request)
    {

        $data = $request->all();

        $id = $data['userid'];
        /** `$id` is the user id */
        $mgt = new \App\PackageMgt;
        $mgt->user_id = $id;
        $mgt->datestart = Carbon::now();
        $mgt->dateend = Carbon::now()->addDays(30);
        $mgt->isCancelled = 0;
        $mgt->package_id = $data['plan'];
        // $mgt->touch();
        $mgt->save();

        return redirect()->route('clientreports')->with('renewed', 'renew success');
    }

    /** Show message inbox,
     * messages just between the clients
     */

     public function showInbox()
     {
         /** Query table `messenger`
          * where `from` = `auth()->user()->id`
          */
        $userid = auth()->user()->id;

        /** Sent messages */
        $Smessages = \App\MessengerModel::where('from','=', $userid)
                    ->paginate(5);

        /** Received messages */
        $Rmessages = \App\MessengerModel::where('to','=', $userid)
                    ->paginate(5);

         return view('inside.admin.inbox', compact('Smessages', 'Rmessages'));
     }

     /** View Message Details */
     public function inboxDetails($id)
     {
         /** Get `Messenger ID` and
          *  Query messenger model */
         $messages = \App\MessengerModel::find($id)->get();

         return view('inside.admin.inboxdetails', compact('messages'));
     }
     /** New Message
      * Redirects to a page where the message can be generated
      */
      public function newMessage($id)
      {
          /** Query table `user` for user subject info */
            $userss = \App\User::where('id', '=', $id)->get();
            // dd($userss);
          return view('inside.admin.newcomposition', compact('userss'));
      }

      /** Save message that will be send */
      public function saveSentMessage(Request $request)
      {
          /** Save message to table `messenger` 
           * then redirect back to inbox
          */
        $data = $request->all();

        $m = new \App\MessengerModel;
        $m->from = auth()->user()->id;
        $m->to = $data['to_id'];
        $m->title = $data['title'];
        $m->content = $data['content'];
        $m->save();

        if (((auth()->user()->type_id)==1) || ((auth()->user()->type_id)==2)) {
            return redirect()->route('showAdminInbox')->with('sent', 'message sent');
        }
        else {
            return redirect()->route('showClientMessages')->with('sent', 'message sent');
        }
        
      }
}
