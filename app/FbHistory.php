<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FbHistory extends Model
{
    /** Initialize table reference */
    protected $table = 'fb_history';

    /** Relatiionship with table `Package_mgt` */
    public function mgt()
    {
        return $this->belongsTo('App\PackageMgt', 'mgt_id');
    }
}
