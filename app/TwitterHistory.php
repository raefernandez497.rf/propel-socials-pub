<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwitterHistory extends Model
{
    /** Initialize table reference */
    protected $table = 'twitter_history';

    /** Relationship with table `Package_mgt`*/
    public function mgt()
    {
        return $this->belongsTo('App\PackageMgt', 'mgt_id');
    }
    
}
