<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessengerModel extends Model
{
    protected $table = 'messenger';

    /** Relationships */
    public function userfrom()
    {
        return $this->belongsTo('App\User', 'from');
    }

    public function userto()
    {
        return $this->belongsTo('App\User', 'to');
    }
}
