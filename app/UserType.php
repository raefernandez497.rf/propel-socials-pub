<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    /** Initialize table reference */
    protected $table = 'user_types';

    /** Initialize custom primary key */
    protected $primaryKey = 'type_id';
}
