<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageMgt extends Model
{
    /** Initialize table reference */
    protected $table = 'package_mgt';

    /** Initialize custom primary key */
    protected $primaryKey = 'mgt_id';

    /** Relationship with table `Packages` */
    public function packages()
    {
        return $this->belongsTo('App\Packages', 'package_id');
    }

    /** Relationship with table `Users` */
    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
