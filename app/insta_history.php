<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class insta_history extends Model
{
    protected $table = 'insta_history';

    public function mgt()
    {
        return $this->belongsTo('App\PackageMgt');
    }
}
