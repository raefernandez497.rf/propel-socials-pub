<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwitterModel extends Model
{
    /** Initialize table reference */
    protected $table = 'twitter';

    /** Initialize custom primary key */
    protected $primaryKey = 'twitter_id';
    
}
