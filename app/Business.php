<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    /** Initialize table reference */
    protected $table = 'business';

    /** Create relationship with Business Types */
    public function business_types()
    {
        return $this->belongsTo('App\BusinessType', 'type_id');
    }
}
