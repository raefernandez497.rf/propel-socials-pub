<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageDetails extends Model
{
    /** Initialize table reference */
    protected $table = 'package_details';

    /** Initialize custom primary key */
    protected $primaryKey = 'detail_id';

    /** Initialize relationship with table `Packages` */
    public function packages()
    {
        return $this->belongsTo('App\Packages', 'package_id');
    }
    
}
