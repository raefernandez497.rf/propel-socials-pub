<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstaModel extends Model
{
    /** Initialize table reference */
    protected $table = 'instagram';

    /** Initialize custom primary key */
    protected $primaryKey = 'inst_id';
}
