<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PackageDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_details', function (Blueprint $table) {
            $table->increments('detail_id');
            $table->tinyinteger('social_creation');
            $table->tinyinteger('content_creation');
            $table->tinyInteger('optimization');
            $table->tinyInteger('strategy');
            $table->tinyInteger('followers_increase');
            $table->tinyInteger('account_mgt');
            $table->tinyInteger('progress_report');
            $table->tinyInteger('no_setup_fee');
            $table->tinyInteger('cancel_anytime');
            $table->tinyInteger('sm_analysis');
            $table->tinyInteger('market_analysis');
            $table->tinyInteger('spam_c_monitoring');
            $table->tinyInteger('ad_conv_analysis');
            $table->timestamps();
        });

        Schema::table('package_details', function (Blueprint $table) {
            $table->unsignedInteger('package_id');
            $table->foreign('package_id')
                    ->references('package_id')->on('packages')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_details');
    }
}
