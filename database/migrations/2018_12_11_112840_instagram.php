<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Instagram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram', function (Blueprint $table) {
            $table->increments('inst_id');
            $table->string('inst_name');
            $table->string('inst_url');
            $table->timestamps();
        });

        Schema::table('user_details', function (Blueprint $table) {
            
            $table->unsignedInteger('inst_id');
            $table->foreign('inst_id')
                    ->references('inst_id')->on('instagram')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram');
    }
}
