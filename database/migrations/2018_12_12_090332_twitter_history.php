<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TwitterHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twitter_history', function (Blueprint $table) {
            $table->increments('id');
            $table->double('engagement');
            $table->double('impressions');
            $table->double('mentions');
            $table->double('followers');
            $table->double('engagement_rating');
            $table->string('datestart');
            $table->string('dateend');
            $table->timestamps();
        });

        Schema::table('twitter_history', function (Blueprint $table) {
            $table->unsignedInteger('mgt_id');
            $table->foreign('mgt_id')
                    ->references('mgt_id')->on('package_mgt')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twitter_history');
    }
}
