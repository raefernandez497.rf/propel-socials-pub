<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PackageMgt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_mgt', function (Blueprint $table) {
            $table->increments('mgt_id');
            $table->string('datestart');
            $table->string('dateend');
            $table->tinyInteger('isCancelled');
            $table->timestamps();
        });

        Schema::table('package_mgt', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');

            $table->unsignedInteger('package_id');
            $table->foreign('package_id')
                    ->references('package_id')->on('packages')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_mgt');
    }
}
