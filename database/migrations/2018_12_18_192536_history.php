<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class History extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_history', function (Blueprint $table) {
            $table->increments('history_id');
            $table->unsignedInteger('mgt_id');            
            $table->unsignedInteger('fb_history_id');
            $table->unsignedInteger('twitter_history_id');
            $table->unsignedInteger('insta_history_id');
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_history');
    }
}
