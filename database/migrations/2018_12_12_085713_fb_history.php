<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FbHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_history', function (Blueprint $table) {
            $table->increments('id');
            $table->double('reach');
            $table->double('engagement');
            $table->double('shares');
            $table->double('ratings');
            $table->string('datestart');
            $table->string('dateend');
            $table->timestamps();
        });

        Schema::table('fb_history', function (Blueprint $table) {
            $table->unsignedInteger('mgt_id');
            $table->foreign('mgt_id')
                    ->references('mgt_id')->on('package_mgt')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_history');
    }
}
