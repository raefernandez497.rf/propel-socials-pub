<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Facebook extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facebook', function (Blueprint $table) {
            $table->increments('fb_id');
            $table->string('fb_name');
            $table->string('fb_url');
            $table->timestamps();
        });

        Schema::table('user_details', function (Blueprint $table) {
            
            $table->unsignedInteger('fb_id');
            $table->foreign('fb_id')
                    ->references('fb_id')->on('facebook')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facebook');
    }
}
