<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BusinessTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_types', function (Blueprint $table) {
            $table->increments('type_id');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });
        
        Schema::table('business', function (Blueprint $table) {
            
            $table->unsignedInteger('business_type');
            $table->foreign('business_type')
                    ->references('type_id')->on('business_types')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_types');                
    }
}
