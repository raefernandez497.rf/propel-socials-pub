<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_types', function (Blueprint $table) {
            $table->increments('type_id');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });
        Schema::table('users', function (Blueprint $table) {
            
            $table->unsignedInteger('type_id');
            $table->foreign('type_id')
                    ->references('type_id')->on('user_types')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_types');        
    }
}
