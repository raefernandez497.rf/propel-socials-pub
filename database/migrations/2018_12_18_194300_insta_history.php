<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstaHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insta_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('datestart');
            $table->string('dateend');
            $table->double('followers');
            $table->double('likes');
            $table->double('mentions');
            $table->double('reach');
            $table->double('engagement');
            $table->double('impressions');
            $table->unsignedInteger('mgt_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insta_history');
    }
}
