<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Twitter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twitter', function (Blueprint $table) {
            $table->increments('twitter_id');
            $table->string('twitter_name');
            $table->string('twitter_url');
            $table->timestamps();
        });

        Schema::table('user_details', function (Blueprint $table) {
            
            $table->unsignedInteger('twitter_id');
            $table->foreign('twitter_id')
                    ->references('twitter_id')->on('twitter')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
