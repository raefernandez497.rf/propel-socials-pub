<!DOCTYPE html>
<html lang="en">
    @include('layouts.header')
<body>
    @if (session('sent'))
        <script>
            swal({
                title: "SUCCESS",
                text: "Message Sent",
                icon: "success",
                button: "Ok",
                });
        </script>
    @endif
    @include('layouts.nav')

    {{-- Body --}}
    <div class="section bg-success text-light wow fadeIn slow">
        <h1 class="text-center" style="font-weight:bold;font-size:400%">Propel Socials</h2>
        <h4 class="text-center">The answer to your social media needs.</h4>
        <div class="row text-center" style="margin-top:3%">
            @guest
                <div class="col-md-6">
                    <a href="{{route('register')}}" class="btn btn-primary btn-lg">
                        Sign Up
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="{{route('login')}}" class="btn btn-info btn-lg">
                        Log In
                    </a>
                </div>
            @else
                <div class="col-md-12">
                    <a href="{{route('showDashboard')}}" class="btn btn-primary btn-lg">
                        Home
                    </a>
                </div>
            @endguest
        </div>
    </div>
    <div class="section bg-light wow fadeIn slow" id="about-us">
        <h2 class="text-center wow fadeIn slow">About Us</h2>
        <p class="text-center service-text wow fadeIn slow">Propel Socials is a social media management business that aims to cater the needs of local businesses in Bukidnon and its neighbouring places with regards to their social media marketing activities. We will provide you with our dedicated services when it comes to creating the contents of the posts and updates for your social media accounts, responding to customers’ comments and messages to the best interest of your business, and providing you with reports and analysis of the trends and of our performance as your social media manager.
        </p>
    </div>
    <div class="section bg-success text-light wow fadeIn slow">
        <h2 class="text-center wow fadeIn slow">Why Choose Us</h2>
        <p class="text-center service-text wow fadeIn slow">Managing your social media accounts that are smartly designed to expose the brand online to its target market and be shared to friends, family and the general public for maximum exposure is what we do.
            The company’s strength lies in its carefully and extensively screened skilled staffs and employees to ensure the best management of our clients’ social media presence
            </p>
            <br>
            <hr>
            <h1 class="text-center wow tada">5 Easy Steps</h1>
        <div class="text-left wow fadeIn slow" style="padding:0%20%">
            <hr class="wow fadeIn slow">

            <h3 class="wow slideInRight slow">Step 1 - CONTACT US</h3>
            <p class="wow fadeIn slower">You can reach us through calling our phone number, sending us a personal message, and visiting us personally at our office.</p>
            <hr class="wow fadeIn slow">
            <h3 class="wow fadeInRight slow">Step 2 - MENTORING AND CONSULTING</h3>
            <p class="wow fadeIn slower">Let us now talk about your business environment so we can provide you with an overview of how we can make things work out for you.</p>
            <hr>
            <h3 class="wow fadeInRight slow">Step 3 - CONTRACT</h3>
            <p class="wow fadeIn slower">Once we had our meeting of the minds, we can agree on what specific subscription package are you going to avail and the time frame of your subscription with our social media management services.</p>
            <hr class="wow fadeIn slow">
            <h3 class="wow fadeInRight slow">Step 4 - SIGN UP </h3>
            <p class="wow fadeIn slower">You’ll need to create your own account so we can update you with our weekly reports and analysis of trends and performance as well as for your billings and other necessary updates.</p>
            <hr class="wow fadeIn slow">
            <h3 class="wow fadeInRight slow">Step 5 - SOCIAL MEDIA MANAGEMENT</h3>
            <p class="wow fadeIn slower">We can now perform our services by giving you only the best for your business.</p>
        </div>
    </div>
    <div class="section text-center">
        <h1 class="wow fadeInUp slow" style="margin-bottom:1%">Let's Get Started</h1>
        <a href="{{ route('register') }}" class="btn btn-primary btn-lg wow fadeInUp slower">Go!</a>
    </div>
    <div class="section bg-success">
        <span class="row" style="margin-left:5%;margin-right:5%;">
            <div class="col-md-6 wow fadeInUp slow">
                <h1 class="text-light" style="font-weight:bold">Send us a message</h1>
                <div class="card">
                    <div class="card-body">
                        <p class="text-gray">You can contact us with anything related to our Services. We'll get in touch with you as soon as possible.</p>
                        <hr>
                        <form action="{{route('sendMessage')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="fullname">Name</label>
                                <input type="text" name="fullname" id="fullname" class="form-control border-success" placeholder="Your Name" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" name="email" id="email" class="form-control border-success" placeholder="Your email" required>
                            </div>
                            <div class="form-group">
                                <label for="contact">Contact</label>
                                <input type="number" name="contact" id="contact" class="form-control border-success" placeholder="Your contact number" required>
                            </div>
                            <div class="form-group">
                                <label for="msg">Your Message</label>
                                <textarea name="msg" id="msg" cols="5" rows="5" class="form-control border-success" placeholder="Your message..." required></textarea>
                            </div>
                            {{-- Recaptcha --}}
                            <script src='https://www.google.com/recaptcha/api.js'></script>
                            <div align="center">
                                <div class="g-recaptcha" data-sitekey="6LeKUYIUAAAAAAOIg7pU5ynOi3L7N8VaG7c8jdbQ"></div>
                                {{-- <div class="g-recaptcha" data-sitekey="6LdRL4EUAAAAAIR93jPTH-eYLec4RUbVUu-wPZY_"></div> --}}
                            </div>
                            <div class="text-center" style="margin-top:3%;">
                                <button class="btn btn-success rounded">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card wow fadeInUp slow" style="margin-bottom:4%">
                    <div class="card-body">
                        <h4>
                            <i class="fas fa-map-marker-alt fa-lg" style="color:tomato"></i>
                            Find us at office
                        </h4>
                        <p class="text-lighter">
                            2nd Floor of Eduave Building, T.N. Pepito Street, Poblacion, Valencia City, Bukidnon.
                        </p>
                        <div class="container">
                            <div class="gmap_canvas">
                                <iframe id="gmap_canvas" width="100%" height="100%" src="https://maps.google.com/maps?q=T.N.%20Pepito%20Street%2C%20Poblacion%2C%20Valencia%20City%2C%20Bukidnon&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                {{-- <a href="https://www.embedgooglemap.net"></a> --}}
                            </div>
                            {{-- <style>
                                .gmap_canvas {
                                    overflow:hidden;
                                    background:none!important;
                                    height:200%!important;
                                    width:200%!important;
                                }
                            </style> --}}
                        </div>
                        {{-- <embed src="https://www.google.com/maps/search/2nd+Floor+of+Eduave+Building,+T.N.+Pepito+Street,+Poblacion,+Valencia+City,+Bukidnon./@7.9075175,125.0893316,17z/data=!3m1!4b1" type=""> --}}
                    </div>
                </div>
                <div class="card wow fadeInUp slower" style="margin-bottom:4%">
                    <div class="card-body">
                        <h4>
                            <i class="fas fa-mobile-alt fa-lg"></i>
                            Call Us
                        </h4>
                            <h5 class="text-lighter">Katharina Lauron</h5>
                        <p class="text-lighter">
                            +63 (935) 940-8190
                        </p>
                    </div>
                </div>
            </div>
        </span>
    </div>
    @include('layouts.footer')
</body>
<script src="{{asset('js/app.js')}}"></script>
</html>