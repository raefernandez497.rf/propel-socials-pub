<div class="footer row">
    <div class="col-md-4">
        <a href="/" class="text-light footer-logo">
            <img src="{{asset('Propel-Socials-Logo-Only.png')}}" width="50" height="50" alt="">
            Propel Socials
        </a>
        <br>
        <span class="text-light">
            Questions? Call Us today!<br>
            +63 (935) 940-8190
        </span>
    </div>
    <div class="col-md-4">
    </div>
    <div class="col-md-4 text-light">
        Follow Us on Social Media!
        <br>
        <a style="margin-right:0.5%" href="https://www.facebook.com/propel-socials" title="Facebook"><i class="fab fa-2x fa-facebook"></i></a>
        <a style="margin-right:0.5%" href="https://www.instagram.com/propel-socials" title="Instagram"><i class="fab fa-2x fa-instagram"></i></a>
        <a style="margin-right:0.5%" href="https://www.twitter.com/propel-socials" title="Twitter"><i class="fab fa-2x fa-twitter-square"></i></a>
        <a style="margin-right:0.5%" href="mailto:propel-socials.co@gmail.com" title="Email"><i class="fas fa-2x fa-envelope"></i></a>
    </div>
</div>