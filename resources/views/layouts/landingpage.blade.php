<!DOCTYPE html>
<!-- saved from url=(0072)https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">















<link rel="apple-touch-icon" sizes="76x76" href="https://demos.creative-tim.com/now-ui-kit-pro/assets/img/apple-icon.png">
<link rel="icon" type="image/png" href="https://demos.creative-tim.com/now-ui-kit-pro/assets/img/favicon.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>





  
    
      Now UI Kit Pro by Creative Tim
    
  

</title>

<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport">



<!--  Social tags      -->
<meta name="keywords" content="bootstrap 4, bootstrap 4 uit kit, bootstrap 4 kit, now ui, now ui kit, creative tim, html kit, html css template, web template, bootstrap, bootstrap 4, css3 template, frontend, responsive bootstrap template, bootstrap ui kit, responsive ui kit, premium bootstrap 4 ui kit, premium template, bootstrap 4 template">
<meta name="description" content="Now UI Kit PRO is a premium Bootstrap 4 kit provided by Invision and Creative Tim. It is a beautiful cross-platform UI kit featuring over 1000 components, 34 sections and 11 example pages.">


<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="Now Ui Kit Pro by Creative Tim">
<meta itemprop="description" content="Now UI Kit PRO is a premium Bootstrap 4 kit provided by Invision and Creative Tim. It is a beautiful cross-platform UI kit featuring over 1000 components, 34 sections and 11 example pages.">

<meta itemprop="image" content="https://s3.amazonaws.com/creativetim_bucket/products/62/opt_nudp_thumbnail.jpg">


<!-- Twitter Card data -->
<meta name="twitter:card" content="product">
<meta name="twitter:site" content="@creativetim">
<meta name="twitter:title" content="Now Ui Kit Pro by Creative Tim">

<meta name="twitter:description" content="Now UI Kit PRO is a premium Bootstrap 4 kit provided by Invision and Creative Tim. It is a beautiful cross-platform UI kit featuring over 1000 components, 34 sections and 11 example pages.">
<meta name="twitter:creator" content="@creativetim">
<meta name="twitter:image" content="https://s3.amazonaws.com/creativetim_bucket/products/62/opt_nudp_thumbnail.jpg">


<!-- Open Graph data -->
<meta property="fb:app_id" content="655968634437471">
<meta property="og:title" content="Now Ui Kit Pro by Creative Tim">
<meta property="og:type" content="article">
<meta property="og:url" content="https://demos.creative-tim.com/now-ui-kit/index.html">
<meta property="og:image" content="https://s3.amazonaws.com/creativetim_bucket/products/62/opt_nudp_thumbnail.jpg">
<meta property="og:description" content="Now UI Kit PRO is a premium Bootstrap 4 kit provided by Invision and Creative Tim. It is a beautiful cross-platform UI kit featuring over 1000 components, 34 sections and 11 example pages.">
<meta property="og:site_name" content="Creative Tim">




<!--     Fonts and icons     -->

  <link href="./Now UI Kit Pro by Creative Tim_files/css" rel="stylesheet">

<link href="./Now UI Kit Pro by Creative Tim_files/all.css" rel="stylesheet">

<!-- CSS Files -->

<link href="./Now UI Kit Pro by Creative Tim_files/bootstrap.min.css" rel="stylesheet">




<link href="./Now UI Kit Pro by Creative Tim_files/now-ui-kit.min.css" rel="stylesheet">





<!-- CSS Just for demo purpose, don't include it in your project -->
<link href="./Now UI Kit Pro by Creative Tim_files/demo.css" rel="stylesheet">


<!-- Visual Inspector Plugin -->
<script async="" src="./Now UI Kit Pro by Creative Tim_files/analytics.js.download"></script><script type="text/javascript" src="./Now UI Kit Pro by Creative Tim_files/vi_dist.min.js.download"></script><script type="text/javascript" async="" src="./Now UI Kit Pro by Creative Tim_files/analytics.js.download"></script><script async="" src="./Now UI Kit Pro by Creative Tim_files/fbevents.js.download"></script><script type="text/javascript" async="" src="./Now UI Kit Pro by Creative Tim_files/ga.js.download"></script><script async="" src="./Now UI Kit Pro by Creative Tim_files/gtm.js.download"></script><script async="" src="./Now UI Kit Pro by Creative Tim_files/vi.min.js.download" charset="UTF-8" crossorigin="*"></script><script type="text/javascript">
  (function (g) {
    var s = document.createElement('script'),
    t = document.getElementsByTagName('script')[0];
    s.async = true;
    s.src = g + '?v=' + (new Date()).getTime();
    s.charset = 'UTF-8';
    s.setAttribute('crossorigin', '*');
    t.parentNode.insertBefore(s, t);
    })('https://www.canvasflip.com/plugins/vi/vi.min.js');
</script>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NKDMSK6');</script>
<!-- End Google Tag Manager -->

    <script type="text/javascript" charset="UTF-8" src="./Now UI Kit Pro by Creative Tim_files/common.js.download"></script><script type="text/javascript" charset="UTF-8" src="./Now UI Kit Pro by Creative Tim_files/util.js.download"></script><link rel="stylesheet" type="text/css" href="./Now UI Kit Pro by Creative Tim_files/background.css"></head>

    <body class="landing-page">
      <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

        <!-- Navbar -->
<nav class="navbar navbar-expand-lg bg-white navbar-absolute navbar-transparent">
	<div class="container">
		
		<div class="dropdown button-dropdown">
			<a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
				<span class="button-bar"></span>
				<span class="button-bar"></span>
				<span class="button-bar"></span>
			</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-header">Dropdown header</a>
				<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#">Action</a>
				<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#">Another action</a>
				<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#">Something else here</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#">Separated link</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#">One more separated link</a>
			</div>
		</div>
		

		<div class="navbar-translate">
			<a class="navbar-brand" href="https://demos.creative-tim.com/now-ui-kit-pro/index.html" rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Designed by Invision. Coded by Creative Tim">
				Now Ui Kit Pro 
			</a>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
		   		<span class="navbar-toggler-bar top-bar"></span>
				<span class="navbar-toggler-bar middle-bar"></span>
				<span class="navbar-toggler-bar bottom-bar"></span>
		 	</button>
		</div>

	    <div class="collapse navbar-collapse show" data-nav-image="../assets/img//blurred-image-1.jpg" data-color="orange">
			<ul class="navbar-nav ml-auto">
			

				<li class="nav-item dropdown">
					<a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink1" data-toggle="dropdown">
						<i class="now-ui-icons design_app"></i>
						<p>Components</p>
					</a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink1">
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/index.html">
							<i class="now-ui-icons business_chart-pie-36"></i>
							All components
						</a>
						<a class="dropdown-item" target="_blank" href="https://demos.creative-tim.com/now-ui-kit-pro/docs/1.0/getting-started/introduction.html">
							<i class="now-ui-icons design_bullet-list-67"></i>
							Documentation
						</a>
					</div>
				</li>

				<li class="nav-item dropdown">
					<a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown">
						<i class="now-ui-icons files_paper" aria-hidden="true"></i>
						<p>Sections</p>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/sections.html#headers">
							<i class="now-ui-icons shopping_box"></i>
							Headers
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/sections.html#features">
							<i class="now-ui-icons ui-2_settings-90"></i>
							Features
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/sections.html#blogs">
							<i class="now-ui-icons text_align-left"></i>
							Blogs
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/sections.html#teams">
							<i class="now-ui-icons sport_user-run"></i>
							Teams
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/sections.html#projects">
							<i class="now-ui-icons education_paper"></i>
							Projects
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/sections.html#pricing">
							<i class="now-ui-icons business_money-coins"></i>
							Pricing
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/sections.html#testimonials">
							<i class="now-ui-icons ui-2_chat-round"></i>
							Testimonials
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/sections.html#contactus">
							<i class="now-ui-icons tech_mobile"></i>
							Contact Us
						</a>
					</div>
				</li>

				<li class="nav-item dropdown">
					<a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown">
						<i class="now-ui-icons design_image" aria-hidden="true"></i>
						<p>Examples</p>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/about-us.html">
							<i class="now-ui-icons business_bulb-63"></i>
							About-us
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/blog-post.html">
							<i class="now-ui-icons text_align-left"></i>
							Blog Post
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/blog-posts.html">
							<i class="now-ui-icons design_bullet-list-67"></i>
							Blog Posts
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/contact-us.html">
							<i class="now-ui-icons location_pin"></i>
							Contact Us
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html">
							<i class="now-ui-icons education_paper"></i>
							Landing Page
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/login-page.html">
							<i class="now-ui-icons users_circle-08"></i>
							Login Page
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/pricing.html">
							<i class="now-ui-icons business_money-coins"></i>
							Pricing
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/ecommerce.html">
							<i class="now-ui-icons shopping_shop"></i>
							Ecommerce Page
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/product-page.html">
							<i class="now-ui-icons shopping_bag-16"></i>
							Product Page
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/profile-page.html">
							<i class="now-ui-icons users_single-02"></i>
							Profile Page
						</a>
						<a class="dropdown-item" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/signup-page.html">
							<i class="now-ui-icons tech_mobile"></i>
							Signup Page
						</a>
					</div>
				</li>

				<li class="nav-item">
					<a class="nav-link btn btn-primary" href="https://www.creative-tim.com/product/now-ui-kit-pro" target="_blank">
						<p>Buy Now</p>
					</a>
				</li>

			
			</ul>
	    </div>
	</div>
</nav>
<!-- End Navbar -->


        
            <div class="wrapper"><div class="page-header page-header-small">

    <div class="page-header-image" data-parallax="true" style="background-image: url(&quot;../assets/img/bg26.jpg&quot;); transform: translate3d(0px, 0px, 0px);">
    </div>
    <div class="content-center">
      <div class="container">
        <h1 class="title">This is our great company.</h1>
        <div class="text-center">
            <a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo" class="btn btn-primary btn-icon btn-round">
                <i class="fab fa-facebook-square"></i>
            </a>
            <a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo" class="btn btn-primary btn-icon btn-round">
                <i class="fab fa-twitter"></i>
            </a>
            <a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo" class="btn btn-primary btn-icon btn-round">
                <i class="fab fa-google-plus"></i>
            </a>
        </div>
      </div>
    </div>

</div>
















<div class="section section-about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                <h2 class="title">Who we are?</h2>
                <h5 class="description">According to the National Oceanic and Atmospheric Administration, Ted, Scambos, NSIDClead scentist, puts the potentially record low maximum sea ice extent tihs year down to low ice extent in the Pacific and a late drop in ice extent in the Barents Sea.</h5>
            </div>
        </div>
        <div class="separator separator-primary"></div>
        <div class="section-story-overview">
            <div class="row">
                <div class="col-md-6">
                    <!-- First image on the left side -->
                    <div class="image-container image-left" style="background-image: url(&#39;../assets/img/bg38.jpg&#39;)">
                        <p class="blockquote blockquote-primary">"Over the span of the satellite record, Arctic sea ice has been declining significantly, while sea ice in the Antarctichas increased very slightly"
                            <br>
                            <br>
                            <small>-NOAA</small>
                        </p>
                    </div>
                    <!-- Second image on the left side of the article -->
                    <div class="image-container image-left-bottom" style="background-image: url(&#39;../assets/img/bg24.jpg&#39;)"></div>
                </div>
                <div class="col-md-5">
                    <!-- First image on the right side, above the article -->
                    <div class="image-container image-right" style="background-image: url(&#39;../assets/img/bg39.jpg&#39;)"></div>
                    <h3>So what does the new record for the lowest level of winter ice actually mean</h3>
                    <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever happens with climate change. Even if the Arctic continues to be one of the fastest-warming regions of the world, it will always be plunged into bitterly cold polar dark every winter. And year-by-year, for all kinds of natural reasons, there’s huge variety of the state of the ice.
                    </p>
                    <p>
                        For a start, it does not automatically follow that a record amount of ice will melt this summer. More important for determining the size of the annual thaw is the state of the weather as the midnight sun approaches and temperatures rise. But over the more than 30 years of satellite records, scientists have observed a clear pattern of decline, decade-by-decade.
                    </p>
                    <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever happens with climate change. Even if the Arctic continues to be one of the fastest-warming regions of the world, it will always be plunged into bitterly cold polar dark every winter. And year-by-year, for all kinds of natural reasons, there’s huge variety of the state of the ice.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="testimonials-1 section-image" style="background-image: url(&#39;../assets/img/bg19.jpg&#39;)">

    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto text-center">
                <h2 class="title">What is ALPHA?</h2>
                <h4 class="description text-white">If you’re selected for ALPHA you’ll also get 3 tickets, opportunity to access Investor Office Hours and Mentor Hours and much more all for €850.</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="card card-testimonial">
                    <div class="card-avatar">
                        <a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo">
                            <img class="img img-raised" src="./Now UI Kit Pro by Creative Tim_files/michael.jpg">
                        </a>
                    </div>
                    <div class="card-body">
                        <p class="card-description">
                            The networking at Web Summit is like no other European tech conference.
                        </p>
                    </div>
                    <div class="icon icon-primary">
                        <i class="fa fa-quote-right"></i>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">Michael Elijah</h4>
                        <p class="category">@michaelelijah</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card card-testimonial">
                    <div class="card-avatar">
                        <a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo">
                            <img class="img img-raised" src="./Now UI Kit Pro by Creative Tim_files/olivia.jpg">
                        </a>
                    </div>
                    <div class="card-body">
                        <p class="card-description">
                            The connections you make at Web Summit are unparalleled, we met users all over the world.
                        </p>
                    </div>
                    <div class="icon icon-primary">
                        <i class="fa fa-quote-right"></i>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">Olivia Harper</h4>
                        <p class="category">@oliviaharper</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card card-testimonial">
                    <div class="card-avatar">
                        <a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo">
                            <img class="img img-raised" src="./Now UI Kit Pro by Creative Tim_files/james.jpg">
                        </a>
                    </div>
                    <div class="card-body">
                        <p class="card-description">
                            Web Summit will increase your appetite, your inspiration, and your network.
                        </p>
                    </div>
                    <div class="icon icon-primary">
                        <i class="fa fa-quote-right"></i>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">James Logan</h4>
                        <p class="category">@jameslogan</p>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<div class="pricing-2">

    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto text-center">
                <h2 class="title">Pick the best plan for you</h2>
                <ul class="nav nav-pills nav-pills-primary justify-content-center" role="tablist">
				  	<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pill1" role="tablist">
							Legal Entity
						</a>
				  	</li>
				  	<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pill2" role="tablist">
							Individual
						</a>
				  	</li>
				</ul>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4">
                <div class="card card-pricing card-plain">
                    <div class="card-body">
                        <h6 class="category">Enterprise</h6>
                        <h1 class="card-title"><small>$</small>59</h1>
                        <ul>
                            <li><b>10GB</b> Disk Space</li>
                            <li><b>100GB</b> Monthly Bandwidth</li>
                            <li><b>20</b> Email Accounts</li>
                            <li><b>Unlimited</b> subdomains</li>
                        </ul>
                        <a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo" class="btn btn-primary btn-round">
                            Sign Up
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card card-pricing card-background card-raised" style="background-image: url(&#39;../assets/img/pricing2.jpg&#39;)">
                    <div class="card-body">
                        <h6 class="category text-info">Professional</h6>
                        <h1 class="card-title"><small>$</small>29</h1>

                        <ul>
                            <li><b>5GB</b> Disk Space</li>
                            <li><b>50GB</b> Monthly Bandwidth</li>
                            <li><b>10</b> Email Accounts</li>
                            <li><b>Unlimited</b> subdomains</li>
                        </ul>
                        <a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo" class="btn btn-neutral btn-round">
                            Sign Up
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card card-pricing card-plain">
                    <div class="card-body">
                        <h6 class="category">Standard</h6>
                        <h1 class="card-title"><small>$</small>17</h1>

                        <ul>
                            <li><b>2GB</b> Disk Space</li>
                            <li><b>25GB</b> Monthly Bandwidth</li>
                            <li><b>5</b> Email Accounts</li>
                            <li><b>Unlimited</b> subdomains</li>
                        </ul>
                        <a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo" class="btn btn-primary btn-round">
                            Get Started
                        </a>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<div class="section section-contact-us text-center">
    <div class="container">
        <h2 class="title">Want to work with us?</h2>
        <p class="description">Your project is very important to us.</p>
        <div class="row">
            <div class="col-lg-6 text-center ml-auto mr-auto col-md-8">
              <div class="input-group input-lg">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="now-ui-icons users_circle-08"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="First Name...">
              </div>

              <div class="input-group input-lg">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="now-ui-icons ui-1_email-85"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Email Here...">
              </div>
                <div class="textarea-container">
                    <textarea class="form-control" name="name" rows="4" cols="80" placeholder="Type a message..."></textarea>
                </div>
                <div class="send-button">
                    <a href="https://demos.creative-tim.com/now-ui-kit-pro/examples/landing-page.html#pablo" class="btn btn-primary btn-round btn-block btn-lg">Send Message</a>
                </div>
            </div>
        </div>
    </div>
</div>


                <footer class="footer footer-default">
    
    <div class="container">
        <nav>
            <ul>
                <li>
                    <a href="https://www.creative-tim.com/">
                        Creative Tim
                    </a>
                </li>
                <li>
                    <a href="http://presentation.creative-tim.com/">
                       About Us
                    </a>
                </li>
                <li>
                    <a href="http://blog.creative-tim.com/">
                       Blog
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright" id="copyright">
            © <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>2018, Designed by <a href="https://www.invisionapp.com/" target="_blank">Invision</a>. Coded by <a href="https://www.creative-tim.com/" target="_blank">Creative Tim</a>.
        </div>
    </div>
    


</footer>

            </div>
        
      















<!--   Core JS Files   -->
<script src="./Now UI Kit Pro by Creative Tim_files/jquery.min.js.download" type="text/javascript"></script>
<script src="./Now UI Kit Pro by Creative Tim_files/popper.min.js.download" type="text/javascript"></script>
<script src="./Now UI Kit Pro by Creative Tim_files/bootstrap.min.js.download" type="text/javascript"></script>

<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="./Now UI Kit Pro by Creative Tim_files/bootstrap-switch.js.download"></script>

<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="./Now UI Kit Pro by Creative Tim_files/nouislider.min.js.download" type="text/javascript"></script>

<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker --><script src="./Now UI Kit Pro by Creative Tim_files/moment.min.js.download"></script>

    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="./Now UI Kit Pro by Creative Tim_files/bootstrap-tagsinput.js.download"></script>
    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="./Now UI Kit Pro by Creative Tim_files/bootstrap-selectpicker.js.download" type="text/javascript"></script>

    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="./Now UI Kit Pro by Creative Tim_files/bootstrap-datetimepicker.js.download" type="text/javascript"></script>

<!--  Google Maps Plugin    -->

<script src="./Now UI Kit Pro by Creative Tim_files/js"></script>


<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc --><script src="./Now UI Kit Pro by Creative Tim_files/now-ui-kit.min.js.download" type="text/javascript"></script>
  <!-- Sharrre libray -->
<script src="./Now UI Kit Pro by Creative Tim_files/jquery.sharrre.js.download"></script>



    


<div class="cf-inspector-edit" id="cfInspectorInlineButton"><div><div></div><div></div></div></div></body></html>