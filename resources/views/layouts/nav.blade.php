<nav class="navbar navbar-dark bg-dark">
    <ul class="nav navbar-nav navbar-left text-left">
        <li class="nav-item text-light" style="float:right">
            {{-- Call Us&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;+63 (935) 940-8190 --}}
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right text-right">
        <li class="nav-item text-light" style="float:right">
            Call Us&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;+63 (935) 940-8190
        </li>
    </ul>
</nav>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/" style="font-weight:bold;font-size:200%">
        <img src="{{asset('Propel-Socials-Logo-Only.png')}}" width="30" height="30" alt="">
        Propel Socials
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <div class="container" style="align-items:right; margin-left:55%">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link nav-header" href="{{ route('about') }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        About
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('about','#our-mission') }}">Our Mission</a>
                        <a class="dropdown-item" href="{{ route('about','#our-vision') }}">Our Vision</a>
                        {{-- <a class="dropdown-item" href="#">Video Features</a>
                        <a class="dropdown-item" href="#">Why You'll Like Us</a>
                        <a class="dropdown-item" href="#">Press and Past Speaking Events</a>
                        <a class="dropdown-item" href="" >Local Services</a> --}}
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link nav-header" href="{{ route('services') }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('services','#mentoring') }}">Mentoring and Consulting</a>
                        <a class="dropdown-item" href="{{ route('services','#management') }}">Social Media Management</a>
                        <a class="dropdown-item" href="{{ route('services','#other') }}">Other Services</a>
                    </div>
                </li>
                {{-- <i class="nav-item nav-header">
                    <a class="nav-link" href="{{ route('services') }}" >Services</a>
                </i> --}}
                <li class="nav-item dropdown">
                    <a class="nav-link nav-header" href="{{ route('our-work') }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Our Work
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/our-work#testimonials">Testimonials</a>
                        <a class="dropdown-item" href="/our-work#portfolio">Portfolio</a>
                    </div>
                </li>
                {{-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Our Work
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li> --}}
                {{-- <i class="nav-item nav-header">
                    <a class="nav-link" href="{{ route('about') }}" >About</a>
                </i>
                <i class="nav-item nav-header">
                    <a class="nav-link" href="{{ route('services') }}" >Services</a>
                </i>
                <i class="nav-item nav-header">
                    <a class="nav-link" href="{{ route('our-work') }}" >Our Work</a>
                </i> --}}
                <li class="nav-item nav-header">
                    <a class="nav-link" href="{{ route('pricing') }}">Pricing</a>
                </li>
                <li class="nav-item nav-header">
                    <span class="form-inline">
                            <a class="nav-link" href="{{route('work-with')}}">Work With Us</a>
                            <i class="fa fa-arrow-right"></i>
                    </span>
                </li>
                {{-- @if (Route::has('login'))
                    @auth
                        <li class="nav-item nav-header">
                            <a href="{{ url('/home') }}" class="nav-link btn btn-primary">Home</a>
                        </li>
                    @else
                        <li class="nav-item nav-header">
                            <a href="{{ route('login') }}" class="nav-link btn btn-success">Login</a>
                        </li>

                        @if (Route::has('register'))
                        <li class="nav-item nav-header">
                            <a href="{{ route('register') }}" class="nav-link">Register</a>
                        </li>
                        @endif
                    @endauth
                @endif --}}
            </ul>
        </div>
        
    </div>
</nav>