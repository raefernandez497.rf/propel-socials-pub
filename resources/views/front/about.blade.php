<!DOCTYPE html>
<html lang="en">
@include('layouts.header')
<body>
    @include('layouts.nav')
    <div class="section bg-success" id="our-mission">
        <h1 class="text-center text-light wow fadeIn" style="font-weight:bolder;">Our Mission</h1>
        <p class="service-text text-light">To extend quality social media management services to its clients while offering affordable service packages to work for every client’s budget. It is Propel Socials’ call to share its love and support for local businesses while being able to become a close partner with its clients, sharing both their passion and business online.</p>
    </div>
    <div class="section" id="our-vision">
        <h1 class="text-center wow fadeIn slower">Our Vision</h1>
        <p class="service-text">A leading social media management business in Bukidnon that offers quality social media management services to its varied business clients composed of teams of dedicated employees where excellence and commitment are the language and culture.</p>
    </div>
    @include('layouts.footer')
</body>
</html>