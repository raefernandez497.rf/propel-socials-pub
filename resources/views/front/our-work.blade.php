<!DOCTYPE html>
<html lang="en">
@include('layouts.header')
<body>
    @include('layouts.nav')
    <div class="section" style="background-color:yellowgreen" id="portfolio">
        <h1 class="text-center text-light wow fadeIn" style="font-weight:bolder;">Our Portfolio</h1>
    </div>
    <div class="section" id="testimonials">
        <h1 class="text-center wow fadeIn" style="font-weight:bolder;">Testimonials</h1>
        
        <div align="center" style="margin-bottom:10%;">
            <span class="row wow fadeInUp slow" style="margin-top:5%;">
                <div class="col-md-6">
                    <img src="{{ asset('ferdi.png') }}" alt="testimonial-1" width="50%" height="100%">
                </div>
                <div class="col-md-6">
                    <span class="row">
                            <p style="margin-right:15%;font-size:180%;"><b style="font-size:250%;font-family:Arial;">&quot;</b><i>If I had to choose one word to describe <b>Propel Socials</b> it would be,’Always’. Always available, always professional, always on time, always budget conscious, always conscientious, and always easy to work with.</i><b style="font-size:250%">&quot;</b>
                                Ferdi L. - <a href="https://www.facebook.com/llasos">Llasos Inc.</a>
                            </p>
                    </span>
                </div>
            </span>
            <span class="row wow fadeInUp slow" style="margin-top:20%;">
                <div class="col-md-6">
                    <span class="row">
                            <p style="margin-left:15%;font-size:180%;"><b style="font-size:250%;font-family:Arial;">&quot;</b><i>Plain and Simple, our creative team would not be what it is today without <b>Propel Socials</b>. They are experienced, energetic, and eager to help. </i><b style="font-size:250%">&quot;</b>
                                <br> Katharina L.- <a href="https://www.facebook.com/lauron">Lauron Services</a>
                            </p>
                    </span>
                </div>
                <div class="col-md-6">
                    <img src="{{ asset('katharina.png') }}" alt="testimonial-1" width="50%" height="100%">
                </div>
            </span>
        </div>
        @guest
            <div align="center">
                <h3 class="wow fadeInUp slow" style="margin-bottom:2%;">Let's Get Started</h3>
                <a href="{{route('register')}}" class="btn btn-primary btn-lg wow fadeInUp slower">
                    Go!
                </a>
            </div>
        @else
            <div align="center" class="wow fadeInUp slower">
                <a href="{{route('showDashboard')}}" class="btn btn-primary btn-lg">
                    Home
                </a>
            </div>
        @endguest
    </div>
    @include('layouts.footer')
</body>
</html>