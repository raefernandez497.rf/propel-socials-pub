<!DOCTYPE html>
<html lang="en">
@include('layouts.header')
<body>
    @include('layouts.nav')
    <div class="section bg-success">
        <span class="row" style="margin-left:5%;margin-right:5%;">
            <div class="col-md-6">
                <div class="card wow fadeInUp slow" style="margin-bottom:4%">
                    <div class="card-body">
                        <h4>
                            <i class="fas fa-map-marker-alt fa-lg" style="color:tomato"></i>
                            Find us at office
                        </h4>
                        <p class="text-lighter">
                            2nd Floor of Eduave Building, T.N. Pepito Street, Poblacion, Valencia City, Bukidnon.
                        </p>
                        <div class="container">
                            <div class="gmap_canvas">
                                <iframe id="gmap_canvas" width="100%" height="100%" src="https://maps.google.com/maps?q=T.N.%20Pepito%20Street%2C%20Poblacion%2C%20Valencia%20City%2C%20Bukidnon&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                {{-- <a href="https://www.embedgooglemap.net"></a> --}}
                            </div>
                            {{-- <style>
                                .gmap_canvas {
                                    overflow:hidden;
                                    background:none!important;
                                    height:200%!important;
                                    width:200%!important;
                                }
                            </style> --}}
                        </div>
                        {{-- <embed src="https://www.google.com/maps/search/2nd+Floor+of+Eduave+Building,+T.N.+Pepito+Street,+Poblacion,+Valencia+City,+Bukidnon./@7.9075175,125.0893316,17z/data=!3m1!4b1" type=""> --}}
                    </div>
                </div>
                <div class="card wow fadeInUp slower" style="margin-bottom:4%">
                    <div class="card-body">
                        <h4>
                            <i class="fas fa-mobile-alt fa-lg"></i>
                            Call Us
                        </h4>
                            <h5 class="text-lighter">Katharina Lauron</h5>
                        <p class="text-lighter">
                            +63 (935) 940-8190
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 wow fadeInUp slow">
                <h1 class="text-light" style="font-weight:bold">Send us a message</h1>
                <div class="card">
                    <div class="card-body">
                        <p class="text-gray">You can contact us with anything related to our Services. We'll get in touch with you as soon as possible.</p>
                        <hr>
                        <form action="{{route('sendMessage')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="fullname">Name</label>
                                <input type="text" name="fullname" id="fullname" class="form-control border-success" placeholder="Your Name" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" name="email" id="email" class="form-control border-success" placeholder="Your email" required>
                            </div>
                            <div class="form-group">
                                <label for="contact">Contact</label>
                                <input type="number" name="contact" id="contact" class="form-control border-success" placeholder="Your contact number" required>
                            </div>
                            <div class="form-group">
                                <label for="msg">Your Message</label>
                                <textarea name="msg" id="msg" cols="5" rows="5" class="form-control border-success" placeholder="Your message..." required></textarea>
                            </div>
                            {{-- Recaptcha --}}
                            <script src='https://www.google.com/recaptcha/api.js'></script>
                            <div align="center">
                                <div class="g-recaptcha" data-sitekey="6LdRL4EUAAAAAIR93jPTH-eYLec4RUbVUu-wPZY_"></div>
                            </div>
                            <div class="text-center" style="margin-top:3%;">
                                <button class="btn btn-success rounded">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </span>
    </div>
    @include('layouts.footer')
    
</body>
</html>