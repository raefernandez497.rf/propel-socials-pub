<!DOCTYPE html>
<html lang="en">
@include('layouts.header')
<body>
    @include('layouts.nav')
    <div class="section bg-success text-light">
        <h1 class="text-center wow fadeInUp slow">Plans & Pricing</h1>
    </div>
    <div class="section">
        <div class="row" style="padding:0%15%0%15%">
            <div class="col-md-4">
                <div class="card pricing gold border-light wow fadeInUp slow">
                    <div class="card-body text-center">
                        <h3 class="pricing-title text-gray">Gold</h3>
                        <br>
                        <i class="fas fa-box fa-3x" style="color:goldenrod;margin-bottom:20%;"></i>
                        <h5><b style="font-weight:bold"> 3,950.00</b>&nbsp;php</h5>
                        <ul>
                            <li>Creation of social media accounts</li>
                            <li>3 posts/week/social network</li>
                            <li>Business Page Optimization</li>
                            <li>Social Meda Strategy</li>
                            <li>Increase in followers</li>
                            <li>Account management</li>
                            <li>Weekly/Monthly Progress Report</li>
                            <li>No Setup Fee</li>
                            <li>Cancel any time</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card pricing platinum border-light wow fadeInUp slow">
                    <div class="card-body text-center">
                        <h3 class="pricing-title text-gray">Platinum</h3>
                        <br>
                        <i class="fas fa-cookie fa-3x" style="color:slategray;margin-bottom:20%;"></i>
                        <h5><b style="font-weight:bold"> 5,750.00</b>&nbsp;php</h5>
                        <ul>
                            <li>Creation of social media accounts</li>
                            <li>5 posts/week/social network</li>
                            <li>Business Page Optimization</li>
                            <li>Social Meda Strategy</li>
                            <li>Increase in followers</li>
                            <li>Account management</li>
                            <li>Weekly/Monthly Progress Report</li>
                            <li>No Setup Fee</li>
                            <li>Cancel any time</li>
                            <li>Social media competitor analysis</li>
                            <li>Market analysis</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 border-light">
                <div class="card pricing diamond border-light wow fadeInUp slow">
                    <div class="card-body text-center">
                        <h3 class="pricing-title text-gray">Diamond</h3>
                        <br>
                        <i class="fas fa-gem fa-3x" style="color:silver;margin-bottom:20%;"></i>
                        <h5><b style="font-weight:bold"> 7,250.00</b>&nbsp;php</h5>
                        <ul>
                            <li>Creation of social media accounts</li>
                            <li>7 posts/week/social network</li>
                            <li>Business Page Optimization</li>
                            <li>Social Meda Strategy</li>
                            <li>Increase in followers</li>
                            <li>Account management</li>
                            <li>Weekly/Monthly Progress Report</li>
                            <li>No Setup Fee</li>
                            <li>Cancel any time</li>
                            <li>Social media competitor analysis</li>
                            <li>Market analysis</li>
                            <li>Spam/comments monitoring</li>
                            <li>Advertising conversion analysis</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div align="center" class="wow fadeInUp slowest">
            @guest
                <div class="col-md-6">
                    <a href="{{route('register')}}" class="btn btn-primary btn-lg">
                        Get Started
                    </a>
                </div>
            @else
                <div class="col-md-12">
                    <a href="{{route('showDashboard')}}" class="btn btn-primary btn-lg">
                        Home
                    </a>
                </div>
            @endguest
        </div>
    </div>
    @include('layouts.footer')
</body>
</html>