<!DOCTYPE html>
<html lang="en">
@include('layouts.header')
<body>
    @include('layouts.nav')
    <div class="section" style="background-color:yellowgreen;">
            <h1 class="text-center text-light wow fadeIn" style="font-weight:bolder;">Services Offered</h1>
    </div>
    <div class="section" id="mentoring">
        <h2 class="text-center wow fadeIn slow">Mentoring & Consulting</h2>
        <p class="text-center service-text wow fadeIn slow">Before any engagement, our team will first talk with you in order for us to assess your business environment and for us to have a clear understanding of your products or services, your business process, and all the necessary information that is vital for us to properly manage your social media accounts.</p>
    </div>
    <div class="section" id="management">
        <h2 class="text-center wow fadeIn">Social Media Management</h2>   
        <p class="text-center service-text wow fadeIn slow">This is the proper discharge of our duties and responsibilities as your social media management team. If you are still to create your business’ social media accounts, our team can also assist you with that. 
                Depending on your subscribed package, we will create the contents (text and graphics) for your social media posts and updates. We will also respond to customer queries according to the best interest of your business. Weekly reports will be fed to your account in order for you to assess how effective are we doing as your social media management team. 
                </p>     
    </div>
    <div class="section text-center" id="other">
        <h2 class="wow fadeIn">Other Services</h2>
        <p class="service-text wow fadeIn slow">If you wish to go with traditional advertising like radio and newspaper advertisements in addition to your social media marketing, we can also assist you in negotiating with local radio stations and newspaper publishers to make things easier for you. Charges will be added upon engaging with this type of services.</p>
        
        <a class="btn btn-primary btn-lg wow fadeIn slow" style="margin-top:5%" href="{{route('register')}}">Get Started</a>
    </div>
    @include('layouts.footer')
</body>
</html>