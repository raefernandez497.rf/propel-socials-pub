@extends('layouts.dashboard')

@section('title')
    Inbox
@endsection

@section('separate')
    @if (session('sent'))
        <script>
            swal({
                title: "Message Sent",
                text: "The message successfully sent.",
                icon: "success",
                button: "Ok",
                });
        </script>
    @endif
    {{-- Sent Messages --}}
    <div class="card">
        <div class="card-header">
            <h5 class="title">Sent Messages</h5>
            {{-- <hr>
            <a href="" class="btn btn-primary">Send Message</a> --}}
            <hr>
        </div>
        <div class="card-body">
        
            <table class="table table-hover">
                <thead>
                    <th>Receiver Name</th>
                    <th>Email</th>
                    <th>Title</th>
                    <th>Content</th>
                </thead>
                <tbody>
                @if (count($Smessages)==0)
                    <tr>
                        <td colspan="4">
                            <div class="alert alert-info text-center">
                                Inbox is empty.
                            </div>
                        </td>
                    </tr>
                @else
                    @foreach ($Smessages as $item)
                    <tr>
                        <td>{{ $item->userto->lname . ', ' . $item->userto->fname }}</td>
                        <td>{{ $item->userto->email }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ substr($item->content, 0, 10) . '...' }} <a href="{{ route('inboxDetails', [ 'id' => $item->id ]) }}">Read more</a></td>
                    </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="container">
                {{ $Smessages->links() }}
            </div>
        </div>
    </div>

    {{-- Received Messages --}}
    <div class="card">
        <div class="card-header">
            <h5 class="title">Received Messages</h5>
        </div>
        <div class="card-body">
        
            <table class="table table-hover">
                <thead>
                    <th>Sender Name</th>
                    <th>Email</th>
                    <th>Title</th>
                    <th>Content</th>
                </thead>
                <tbody>
                @if (count($Rmessages)==0)
                    <tr>
                        <td colspan="4">
                            <div class="alert alert-info text-center">
                                Inbox is empty.
                            </div>
                        </td>
                    </tr>
                @else
                    @foreach ($Rmessages as $item)
                    <tr>
                        <td>{{ $item->userfrom->lname . ', ' . $item->userfrom->fname }}</td>
                        <td>{{ $item->userfrom->email }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ substr($item->content, 0, 10) . '...' }} <a href="{{ route('inboxDetails', [ 'id' => $item->id ]) }}">Read more</a></td>
                    </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="container">
                {{ $Rmessages->links() }}
            </div>
        </div>
    </div>
@endsection