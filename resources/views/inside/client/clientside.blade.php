@if ((substr( url()->current(), 21,20))=='/client/dashboard')
    <li class="active">
@else
    <li>
@endif
    <a href="{{route('showDashboard')}}">
    <i class="now-ui-icons design_app"></i>
    <p>Dashboard</p>
    </a>
</li>

@if ((substr( url()->current(), 21,20))=='/client/profile')
    <li class="active">
@else
    <li>
@endif
    <a href="{{route('profile')}}">
    <i class="now-ui-icons business_badge"></i>
    <p>Profile</p>
    </a>
</li>

@if ((substr( url()->current(), 21,20))=='/client/reports')
    <li class="active">
@else
    <li>
@endif
    <a href="{{route('clientreports')}}">
    <i class="now-ui-icons business_badge"></i>
    <p>Reports</p>
    </a>
</li>

@if (((substr( url()->current(), 27,20))=='/client/messages'))
    <li class="active">
@else
    <li>
@endif
{{-- now-ui-icons users_single-02 --}}
    <a href="{{route('showClientMessages')}}">
    <i class="fas fa-comment"></i>
    <p>Messages</p>
    </a>
</li>