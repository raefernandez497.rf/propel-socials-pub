@extends('layouts.dashboard')

{{-- @section('top-header')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
@endsection --}}
@section('title')
    Reports
@endsection

@section('separate')
    @if (session('success'))
        <script>
            swal({
                title: "SUCCESS",
                text: "Successfully subscribed to a package!",
                icon: "success",
                button: "Ok",
                });
        </script>
    @endif
    @if (session('noreport'))
        <div class="card">
            <div class="card-body">
                @if ($mgtdetails==0)
                    <div class="card">
                        <div class="card-body">
                            <div class="alert alert-danger">
                                You have no current package subscription!
                            </div>
                        </div>
                    </div>
                @else
                    <div class="alert alert-info">
                        No current reports.
                        <a href="#" class="btn btn-primary">Click here to renew.</a>
                    </div>
                @endif
            </div>
        </div>
    @elseif(!session('noreport'))
        @php
        foreach ($udetails as $detail) {
            $info = [
                'fb_name' => $detail->fb->fb_name,

            ];
        }

        foreach ($mgtdetails as $item) {
            $mgt = [
                'package_name' => $item->packages->name,
                'date' => $item->datestart . ' to ' . $item->dateend,
            ]; 
        }
        if (count($facebook)!=0) {
            foreach ($facebook as $f) {
                $fb = [
                    'reach' => $f->reach,
                    'engagement' => $f->engagement,
                    'shares' => $f->shares,
                    'ratings' => $f->ratings,
                ];
            }
        }

        if (count($twitter)!=0) {
            foreach ($twitter as $t) {
                $tw = [
                    'engagement' => $t->engagement,
                    'impressions' => $t->impressions,
                    'mentions' => $t->mentions,
                    'followers' => $t->followers,
                    'eng_rating' => $t->engagement_rating,
                    'datestart' => $t->datestart,
                    'dateend' => $t->dateend,
                ];
            }
        }    

        // if ($instagram->count()!=0) {
            
        // }
        @endphp
        <div>
            @if ((count($mgtdetails))==0)
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-danger">
                            You have no current package subscription!
                            <p>Click <a href="{{ route('renewRedirect') }}">here</a> to have a new package subscription.</p>
                        </div>
                    </div>
                </div>
            @else
                @if (count($rhistory)==0)
                    <div class="card">
                        <div class="card-body">
                            <div class="alert alert-info">
                                <p>There are no current reports at the moment.</p>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">
                                Weekly Performance Report
                            </h5>
                        </div>
                        <div class="card-body">
                            <h5 class="description">Subscription Package:&nbsp;{{ $mgt['package_name'] }}</h5>
                        </div>
                    </div>

                    <span class="row">
                        @if ((count($facebook))!=0)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="title">
                                            <i class="fab fa-facebook" style="color:"></i>
                                            Facebook
                                        </h5>
                                    </div>
                                    <div class="card-body">
                                        <div>
                                            <div class="form-group">
                                                <h5>Subscription Period:</h5>
                                                <p>{{ $mgt['date'] }}</p>
                                            </div>
                                            <hr>
                                            <h5>Reach: <u>{{$fb['reach']}}</u></h5>
                                            <h5>Engagement: <u>{{$fb['engagement']}}</u></h5>
                                            <h5>Shares: <u>{{$fb['shares']}}</u></h5>
                                            <h5>Average Ratings: <u>{{$fb['ratings']}}</u></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="alert alert-info text-center">
                                            <i class="fab fa-facebook" style="font-size:200%;"></i>
                                            <p>There are no current report statistics on facebook.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if (count($twitter)!=0)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="title">
                                            <i class="fab fa-twitter" style="color:dodgerblue"></i>
                                            Twitter
                                        </h5>
                                    </div>
                                    <div class="card-body">
                                        <div>
                                            <div class="form-group">
                                                <h5>Subscription Period:</h5>
                                                <p>{{ $tw['datestart'] . ' to ' . $tw['dateend'] }}</p>
                                            </div>
                                            <hr>
                                            <h5>Impressions: <u>{{ $tw['impressions'] }}</u></h5>
                                            <h5>Followers: <u>{{ $tw['followers'] }}</u></h5>
                                            <h5>Engagement: <u>{{ $tw['engagement'] }}</u></h5>
                                            <h5>Engagement Rating: <u> {{ $tw['eng_rating'] }}</u></h5>
                                            <h5>Mentions: <u>{{ $tw['mentions'] }}</u></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="alert alert-info text-center">
                                            <i class="fab fa-twitter" style="font-size:200%;"></i>
                                            <p>There are no current report statistics on twitter.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if (count($instagram)!=0)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="title">
                                            Instagram
                                        </h5>
                                    </div>
                                    <div class="card-body">
                    
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="alert alert-info text-center">
                                            <i class="fab fa-instagram" style="font-size:200%;"></i>
                                            <p>There are no current report statistics on instagram.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </span>
                    {{-- Message, comes out when filled--}}
                    @if ($message!='')
                        <div class="card">
                            <div class="card-header">
                                <p class="description">Business Implications</p>
                            </div>
                            <div class="card-body">
                                <p style="text-align:justify;">{{ $message }}</p>
                            </div>
                        </div>
                    @endif
                @endif
            @endif
        </div>
    @endif
@endsection