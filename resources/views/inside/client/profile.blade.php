@extends('layouts.dashboard')

@section('title')
    Profile
@endsection

@section('top-header')
    @if (session('success'))
        <script>
            swal({
                title: "SUCCESS",
                text: "Profile successfully updated!",
                icon: "success",
                button: "Ok",
                });
        </script>
    @endif    
@endsection

@section('separate')
@php
    foreach($details as $detail)
    {
        $info = [
            'fullname' => $detail->users->lname . ', ' . $detail->users->fname . ' ' . substr($detail->users->mname,0,1) . '.',
            'email' => $detail->users->email,
            'contact' => $detail->users->contactNo,
            'bname' => $detail->business->business_name,
            'baddress' => $detail->business->business_address,
            'established' => $detail->business->date_established,
            'sex' => $detail->users->sex,
            'bday' => $detail->users->birthday,
        ];
    }
@endphp
    <span class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="title">
                        {{ $info['fullname'] }}
                        @if ($info['sex']=='Male')
                            <i class="fas fa-male"></i>
                        @elseif($info['sex']=='Female')
                            <i class="fas fa-female"></i>
                        @endif
                    </h5>
                    <p style="font-weight:bold">Contact Details:</p>
                    <p class="description">{{ $info['contact'] }}</p>
                    <p class="description">{{ $info['email'] }}</p>
                    <hr>
                    <h6 class="title">Business Details:</h6>
                    <b>Name: </b><p class="description">{{ $info['bname'] }}</p>
                    <b>Address: </b><p class="description">{{ $info['baddress'] }}</p>
                    <b>Date Established: </b><p class="description">{{ $info['established'] }}</p>

                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Edit Profile</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('saveClientProfile') }}" method="post">
                        @csrf
                        <span class="row">
                            <div class="form-group col-md-4">
                                <label for="fname">First Name:</label>
                                <input type="text" name="fname" id="fname" class="form-control" value="{{auth()->user()->fname}}" placeholder="Your First Name">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="mname">Middle Name:</label>
                                <input type="text" name="mname" id="mname" class="form-control" value="{{auth()->user()->mname}}" placeholder="Your Middle Name">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="lname">Last Name:</label>
                                <input type="text" name="lname" id="lname" class="form-control" value="{{auth()->user()->lname}}" placeholder="Your Last Name">
                            </div>
                        </span>
                        <hr>
                        <h5>Business</h5>
                        <span class="row">
                            <div class="form-group col-md-4">
                                <label for="bname">Name:</label>
                                <input type="text" name="bname" id="bname" class="form-control" value="{{ $info['bname'] }}" placeholder="Business Name">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="bname">Established</label>
                                <input type="text" name="established" id="established" class="form-control" value="{{ $info['established'] }}" placeholder="Business Name">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="baddress">Business Address:</label>
                                <textarea name="baddress" id="baddress" class="form-control" placeholder="Business Address">{{ $info['baddress'] }}</textarea>
                            </div>
                        </span>
                        <div align="center">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </span>
@endsection