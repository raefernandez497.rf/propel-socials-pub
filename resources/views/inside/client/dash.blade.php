@extends('layouts.dashboard')

@section('title')
    Dashboard
@endsection

@section('content')
    <div class="card-header">
        <h5 class="title">Welcome</h5>
    </div>
    <div class="card-body">
        <h6 class="title">Quick Links</h6>
        <hr>
            <a href="{{ route('profile') }}" class="btn btn-primary" title="Show client list">Profile</a>
            <a href="{{route('clientreports')}}" class="btn btn-primary" title="Show reports">Reports</a>
        <hr>
        
    </div>
@endsection