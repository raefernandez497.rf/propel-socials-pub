@extends('layouts.dashboard')

@section('title')
    Clients
@endsection

@section('content')
@php
    foreach ($details as $detail) {
        $info = [
            'fullname' => $detail->users->lname . ', ' . $detail->users->fname . ' ' . substr($detail->users->mname,0,1) . '.',
            'email' => $detail->users->email,
            'contact' => $detail->users->contactNo,
            'bname' => $detail->business->business_name,
            'baddress' => $detail->business->business_address,
            'established' => $detail->business->date_established,
        ];
    }
    foreach ($mgts as $item) {
        $packagename = $item->packages->name;
    }
@endphp
    <div class="card-header">
        <h5 class="title">Details</h5>
        <hr>
        {{-- get the id for the routing and client info --}}
        <a href="{{route('showReports', ['id' => $id])}}" class="btn btn-primary">
            Reports
        </a>
        <hr>
    </div>
    <div class="card-body">
        <h5><b>Package:</b>&nbsp;{{$packagename}}</h5>
        <h5><b>Client Name:</b>&nbsp;{{ $info['fullname'] }}</h5>
        <h5><b>Business Name:</b>&nbsp;{{ $info['bname'] }}</h5>
        <h5><b>Business Address:</b>&nbsp;{{ $info['baddress'] }}</h5>
        <h5><b>Date Established:&nbsp;</b>{{ \Carbon\Carbon::parse($info['established'])->format('F d Y') }}</h5>
        <hr>
        <h5 class="text-center"><b>Subscription History</b></h5>
        <hr>
        <table class="table table-hover">
            <thead>
                <th>Package Name</th>
                <th>Price</th>
                <th>Expiration Date</th>
            </thead>
            <tbody>
                @foreach ($subhist as $item)
                    <tr>
                        <td>{{ $item->packages->name }}</td>
                        <td>{{ $item->packages->price . '.00 PHP'}}</td>
                        <td>{{ \Carbon\Carbon::parse($item->dateend)->toDateString() }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="container" style="margin-left:40%">
            {{ $subhist->links() }}
        </div>
    </div>
@endsection