@extends('layouts.dashboard')

@section('title')
    Message
@endsection
@php
    foreach($messages as $item) {
        $info = [
            'name' => $item->name,
            'email' => $item->email,
            'contactno' => $item->contact_no,
            'message' => $item->message,
            'datesent' => \Carbon\Carbon::parse($item->created_at)->toFormattedDateString(),
        ];
    }
@endphp
@section('separate')
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Details</h5>
                <hr>
            </div>
            <div class="card-body">
                <div>
                    <h5><b>Date Sent: </b> {{$info['datesent']}}</h5>
                    <h5><b>Sender: </b>{{$info['name']}}</h5>
                    <h5><b>Contact Number: </b>{{$info['contactno']}}</h5>
                    <h5><b>Email Address: </b>{{$info['email']}}</h5>
                    <h5><b>Message:</b></h5>
                    <p style="text-align:justify;">{{$info['message']}}</p>
                </div>
                <div class="text-center">
                    <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection