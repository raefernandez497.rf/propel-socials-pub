@if (((substr( url()->current(), 27,20))=='/admin/dashboard'))
    <li class="active">
@else
    <li>
@endif
    <a href="{{route('showDashboard')}}">
    <i class="now-ui-icons design_app"></i>
    <p>Dashboard</p>
    </a>
</li>

@if (((substr( url()->current(), 27,20))=='/admin/profile'))
    <li class="active">
@else
    <li>
@endif
    <a href="{{route('profile')}}">
    <i class="now-ui-icons business_badge"></i>
    <p>Profile</p>
    </a>
</li>
@if (((substr( url()->current(), 27,20))=='/admin/packages'))
    <li class="active">
@else
    <li>
@endif
    <a href="{{route('showPackages')}}">
    <i class="now-ui-icons files_box"></i>
    <p>Packages</p>
    </a>
</li>
@if (((substr( url()->current(), 27,20))=='/admin/clients'))
    <li class="active">
@else
    <li>
@endif
    <a href="{{route('showClients')}}">
    <i class="now-ui-icons design_bullet-list-67"></i>
    <p>Clients</p>
    </a>
</li>
@if (((substr( url()->current(), 27,20))=='/admin/users'))
    <li class="active">
@else
    <li>
@endif
    <a href="{{route('showUsers')}}">
    <i class="now-ui-icons users_single-02"></i>
    <p>Users</p>
    </a>
</li>

@if (((substr( url()->current(), 27,20))=='/admin/messages'))
    <li class="active">
@else
    <li>
@endif
{{-- now-ui-icons users_single-02 --}}
    <a href="{{route('showMessages')}}">
    <i class="fas fa-comment"></i>
    <p>Messages</p>
    </a>
</li>