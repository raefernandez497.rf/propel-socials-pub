@extends('layouts.dashboard')
@php
    foreach($details as $detail)
    {
        $info = [
            'fullname' => $detail->users->lname . ', ' . $detail->users->fname . ' ' . substr($detail->users->mname,0,1) . '.',
            'email' => $detail->users->email,
            'contact' => $detail->users->contactNo,
            'bname' => $detail->business->business_name,
            'baddress' => $detail->business->business_address,
            'established' => $detail->business->date_established,
            'uid' => $detail->users->id,
            'fbid' => $detail->fb_id,
            'twid' => $detail->twitter_id,
            'instid' => $detail->inst_id,
        ];
    }
@endphp
@section('title')
    <h5 class="title">Reports</h5>
    <div>
            <p style="text-transform:none"><b>Client Name:</b>&nbsp;<u>{{ $info['fullname'] }}</u></p>
            <br>
            <p style="text-transform:none"><b>Business Name:</b><u>&nbsp;{{ $info['bname'] }}</u></p> 
    </div>
@endsection

@if (!(session('error')))
    @section('separate')

        @if (session('error'))
            <script>
                swal({
                    title: "NO SUBSCRIPTION",
                    text: "Client has no current package subscriptions!",
                    icon: "warning",
                    button: "Ok",
                    });
            </script>
        @endif
        @if (session('success'))
            <script>
                swal({
                    title: "SUCCESS",
                    text: "Report successfully added!",
                    icon: "success",
                    button: "Ok",
                    });
            </script>
        @endif
        <div class="card" style="margin-top:1%;">
            <div class="card-header">
                <h5 class="title row">
                    <div class="col-md-6">
                        Package: {{ $pname[0] }}
                    </div>
                    <div class="col-md-6">
                        Expiry: {{ \Carbon\Carbon::parse($xpiry[0])->toDateString() }}
                    </div>
                </h5>
            </div>
        </div>
    <form action="{{route('saveReport')}}" method="post">
        @csrf
        <input type="hidden" name="userid" value="{{$info['uid']}}">
    <div class="row">
        {{-- For Facebook Card --}}
        @if ($info['fbid']!='')
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <i class="fab fa-facebook-alt"></i>
                        <h5 class="title">Facebook</h5>
                    </div>
                    <div class="card-body">
                        <span>
                            <div class="form-group">
                                <label for="fdatestart" class="col-form-label text-md-right">Start Date:</label>
                                <input type="date" name="fdatestart" id="fdatestart" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="fdateend" class="col-form-label text-md-right">End Date:</label>
                                <input type="date" name="fdateend" id="fdateend" class="form-control">
                            </div>
                        </span>
                        <hr>
                        <span class="row">
                            <div class="form-group col-md-6">
                                <label for="reach" class="col-form-label text-md-right">Reach:</label>
                                <input type="number" name="reach" id="reach" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="fengagement" class="col-form-label text-md-right">Engagement:</label>
                                <input type="number" name="fengagement" id="fengagement" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="shares" class="col-form-label text-md-right">Shares:</label>
                                <input type="number" name="shares" id="shares" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ratings" class="col-form-label text-md-right">Ratings:</label>
                                <input type="number" name="ratings" id="ratings" class="form-control">
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        @endif
        {{-- For Twitter Card --}}
        @if ($info['twid']!='')
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">
                            <i class="fab fa-twitter-square-alt"></i>
                            Twitter
                        </h5>
                    </div>
                    <div class="card-body">
                        <span>
                            <div class="form-group">
                                <label for="tdatestart" class="col-form-label text-md-right">Start Date:</label>
                                <input type="date" name="tdatestart" id="tdatestart" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="tdateend" class="col-form-label text-md-right">End Date:</label>
                                <input type="date" name="tdateend" id="tdateend" class="form-control">
                            </div>
                        </span>
                        <hr>
                        <span class="row">
                            <div class="form-group col-md-6">
                                <label for="followers" class="col-form-label text-md-right">Followers</label>
                                <input type="number" name="followers" id="followers" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mentions" class="col-form-label text-md-right">Mentions</label>
                                <input type="number" name="mentions" id="mentions" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="tengagement" class="col-form-label text-md-right">Engagement</label>
                                <input type="number" name="tengagement" id="tengagement" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="eng_rating" class="col-form-label text-md-right">Engagement Rating</label>
                                <input type="text" name="eng_rating" id="eng_rating" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="impressions" class="col-form-label text-md-right">Impressions</label>
                                <input type="number" name="impressions" id="impressions" class="form-control">
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        @endif
        @if ($info['instid']!=0)
            <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">
                                Instagram
                            </h5>
                        </div>
                        <div class="card-body">
                            <span>
                                <div class="form-group">
                                    <label for="idatestart" class="col-form-label text-md-right">Start Date:</label>
                                    <input type="date" name="idatestart" id="idatestart" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="idateend" class="col-form-label text-md-right">End Date:</label>
                                    <input type="date" name="idateend" id="idateend" class="form-control">
                                </div>
                            </span>
                            <hr>
                            <span class="row">
                                <div class="form-group col-md-6">
                                    <label for="ifollowers" class="col-form-label text-md-right">Followers</label>
                                    <input type="number" name="ifollowers" id="ifollowers" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="imentions" class="col-form-label text-md-right">Mentions</label>
                                    <input type="number" name="imentions" id="imentions" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="ireach" class="col-form-label text-md-right">Reach</label>
                                    <input type="number" name="ireach" id="ireach" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="iengagement" class="col-form-label text-md-right">Engagement</label>
                                    <input type="number" name="iengagement" id="iengagement" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="iimpressions" class="col-form-label text-md-right">Impressions</label>
                                    <input type="number" name="iimpressions" id="iimpressions" class="form-control">
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    <div class="card">
        <div class="card-body">
            <label for="message">Message</label>
            <textarea name="message" class="form-control"></textarea>
        </div>
    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    </form>

    {{-- <div class="card-header">
        <h5 class="title">Reports for Client:</h5>
        Client Name
    </div>
    <div class="card-body">
        
    </div> --}}
    @endsection
@endif