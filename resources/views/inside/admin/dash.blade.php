@extends('layouts.dashboard')

@section('title')
    Dashboard
@endsection
@section('content')
    <div class="card-header">
        <h5 class="title">Welcome</h5>
    </div>
    <div class="card-body">
        <h6 class="title">Quick Links</h6>
        <hr>
            <a href="{{ route('showClients') }}" class="btn btn-primary" title="Show client list">Clients</a>
            <a href="{{ route('showUsers') }}" class="btn btn-primary" title="Show user list">Users</a>
            <a href="{{ route('showPackages') }}" class="btn btn-primary" title="Show package list">Packages</a>
            <a href="{{ route('showMessages') }}" class="btn btn-primary" title="Show package list">Messages</a>
        <hr>
        
    </div>
@endsection