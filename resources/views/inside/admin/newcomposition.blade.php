@extends('layouts.dashboard')

@section('title')
    Inbox
@endsection

@php
    foreach($userss as $user)
    {
        $info = [
            'to' => $user->lname . ', ' . $user->fname,
            'to_id' => $user->id,
        ];
    }
@endphp

@section('separate')
    <div class="col-md-6">
            <div class="card">
                    <div class="card-header">
                        <h5 class="title">
                            Send Message
                        </h5>
                    </div>
                    <div class="card-body">
                        <div>
                            <h5>Message will be sent to: <b>{{$info['to']}}</b></h5>
                            <form action="{{ route('saveComposition') }}" method="post">
                                @csrf
                                <input type="hidden" name="to_id" value="{{$info['to_id']}}">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" id="title" class="form-control col-md-10" placeholder="Message Title">
                                </div>
                                <div class="form-group">
                                    <label for="content">Message</label>
                                    <textarea name="content" id="content" rows="5" class="form-control col-md-10" placeholder="Your Message"></textarea>
                                </div>
                                <div class="text-center">
                                    <a href="{{ url()->previous() }}"><i class="fas fa-angle-left"></i> Back</a>
                                    <button type="submit" class="btn btn-primary" style="margin-left:2%;">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
    </div>
@endsection