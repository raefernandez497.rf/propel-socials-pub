@extends('layouts.dashboard')

@section('title')
    Inbox
@endsection

@php
    foreach($messages as $item) {
        $info = [
            'to_id' => $item->to,
            'from_id' => $item->from,
            'nameto' => $item->userto->lname . ', ' . $item->userto->fname,
            'namefrom' => $item->userfrom->lname . ', ' . $item->userfrom->fname,
            'emailfrom' => $item->userfrom->email,
            'emailto' => $item->userto->email,
            'fromcontactno' => $item->userfrom->contactNo,
            'tocontactno' => $item->userto->contactNo,
            'title' => $item->title,
            'message' => $item->content,
            'datesent' => \Carbon\Carbon::parse($item->created_at)->toFormattedDateString(),
        ];
    }
@endphp

@section('separate')
    <div class="col-md-6">
            <div class="card">
                    <div class="card-header">
                        <h5 class="title">
                            Details
                        </h5>
                        <hr>
                    </div>
                    <div class="card-body">
                        <div>
                            <h5><b>Date Sent: </b> {{$info['datesent']}}</h5>
                            @if (auth()->user()->id==$info['from_id'])
                                <h5><b>Receiver: </b>{{$info['nameto']}}</h5>
                                <h5><b>Contact Number: </b>{{$info['tocontactno']}}</h5>
                                <h5><b>Email Address: </b>{{$info['emailto']}}</h5>
                            @else
                                <h5><b>Sender: </b>{{$info['namefrom']}}</h5>
                                <h5><b>Contact Number: </b>{{$info['fromcontactno']}}</h5>
                                <h5><b>Email Address: </b>{{$info['emailfrom']}}</h5>
                            @endif
                            <hr>
                            <h5><b>Subject: </b></h5>
                                <p style="text-align:justify;">{{$info['title']}}</p>            
                            <h5><b>Message:</b></h5>
                                <p style="text-align:justify;">{{$info['message']}}</p>
                        </div>
                        <div class="text-center">
                            <a href="{{ url()->previous() }}" style="margin-right:2%;">
                                <i class="fas fa-angle-left"></i>
                                Back</a>
                            {{-- If ikaw ang nag send --}}
                            @if (auth()->user()->id==$info['from_id'])
                                <a href="{{ route('newComposition', [ 'id' => $info['to_id'] ] ) }}" class="btn btn-primary">Send Message</a>
                            {{-- If ang message kay gikan sa uban user --}}
                            @else
                                <a href="{{ route('newComposition', [ 'id' => $info['from_id'] ] ) }}" class="btn btn-primary">Reply</a>
                            @endif
                        </div>
                    </div>
                </div>
    </div>
@endsection