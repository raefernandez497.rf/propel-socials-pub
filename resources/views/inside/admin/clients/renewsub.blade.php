@extends('layouts.dashboard')

@section('title')
    Package Subscription
@endsection

@section('content')
    <div class="card-header">
        <h5 class="title">
            Renew Package Subscription
        </h5>
    </div>
    <div class="card-body">
        <div>
            <form action="{{ route('renewSub') }}" method="post">
                @csrf
                <input type="hidden" name="userid" value="{{ auth()->user()->id }}">
                <div class="form-group row">
                    <label for="ptype" class="col-form-label text-md-right col-md-3">Package Type: </label>
                    <div class="col-md-4">
                        <select name="ptype" id="ptype" class="form-control">
                            @foreach ($packages as $item)
                                <option value="{{ $item->package_id }}">{{ $item->name . ' - ' . $item->price . '.00 PHP' }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection