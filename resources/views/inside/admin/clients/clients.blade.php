@extends('layouts.dashboard')

@section('title')
    <h5 class="title">Clients</h5>
@endsection

@section('below-body')
    @if (session('message'))
    <script>
        swal({
            title: "Select Client",
            text: "Please select the client for the message.",
            icon: "info",
            button: "Ok",
            });
    </script>
    @endif    
@endsection

@section('content')
    <div class="card-header">
        <h5 class="title">Client List</h5>
        <hr>
        {{-- <a href="#" class="btn btn-primary">
            New Client
        </a>
        <hr> --}}
    </div>
    <div class="card-body">
        @if (($details->count())!=0)
            <table class="table table-hover table-striped">
                <thead>
                    <th>Name</th>
                    <th>Business Name</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($details as $user)
                    <tr>
                            <td>{{ $user->users->lname . ', ' . $user->users->fname . ' ' . substr($user->users->lname, 0,1) }}</td>
                            <td>{{ $user->business->business_name }}</td>
                            <td>
                                <a href="{{route('showDetails', ['id' => $user->users->id ])}}" class="btn btn-primary">
                                    Details
                                </a>
                                <a href="{{ route('newComposition', [ 'id' => $user->users->id ]) }}" class="btn btn-info">
                                    <i class="fas fa-paper-plane"></i>
                                    &nbsp;Send Message</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- Pagination --}}
            <div class="container" style="margin-left:40%">
                {{ $details->links() }}
            </div>
        @else
            <div class="alert alert-warning" role="alert">
                There are no current clients!
            </div>
        @endif
    </div>
@endsection