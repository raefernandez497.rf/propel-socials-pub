@extends('layouts.dashboard')

@section('title')
    Profile
@endsection
@section('separate')
    @if (session('success'))
        <script>
            swal({
                title: "SUCCESS",
                text: "Profile successfully updated!",
                icon: "success",
                button: "Ok",
                });
        </script>
    @endif
@php
    foreach($users as $user)
    {
        $info = [
            'fullname' => $user->lname . ', ' . $user->fname . ' ' . substr($user->mname,0,1) . '.',
            'email' => $user->email,
            'contact' =>$user->contactNo,
            'sex' => $user->sex,
            'bday' => $user->birthday,
        ];
    }
@endphp
    <span class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Edit Profile</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('updateAdmin') }}" method="post">
                        @csrf
                        <input type="hidden" name="userid" value="{{ auth()->user()->id }}">
                        <span>
                            <div class="form-group col-md-12">
                                <label for="fname">First Name:</label>
                                <input type="text" name="fname" id="fname" class="form-control" value="{{auth()->user()->fname}}" placeholder="Your First Name">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="mname">Middle Name:</label>
                                <input type="text" name="mname" id="mname" class="form-control" value="{{auth()->user()->mname}}" placeholder="Your Middle Name">
                            </div>
                        </span>

                        <span>
                            <div class="form-group col-md-12">
                                <label for="lname">Last Name:</label>
                                <input type="text" name="lname" id="lname" class="form-control" value="{{auth()->user()->lname}}" placeholder="Your Last Name">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="contactno">Contact Number:</label>
                                <input type="text" name="contactno" id="contactno" class="form-control" value="{{auth()->user()->contactNo}}" placeholder="Your Contact Number">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="email">Email:</label>
                                <input type="text" name="email" id="email" class="form-control" value="{{auth()->user()->email}}" placeholder="Your Email">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="oldpass">Old Password:</label>
                                <input type="password" name="oldpass" id="oldpass" class="form-control" placeholder="Your Old Password">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="newpass">New Password:</label>
                                <input type="password" name="newpass" id="newpass" class="form-control" placeholder="Your New Password">
                            </div>
                        </span>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="title">
                        {{ $info['fullname'] }}
                        @if ($info['sex']=='Male')
                            <i class="fas fa-male"></i>
                        @elseif($info['sex']=='Female')
                            <i class="fas fa-female"></i>
                        @endif
                    </h5>
                    <p style="font-weight:bold">Contact Details:</p>
                    <p class="description">{{ $info['contact'] }}</p>
                    <p class="description">{{ $info['email'] }}</p>
                </div>
            </div>
        </div>
    </span>
@endsection