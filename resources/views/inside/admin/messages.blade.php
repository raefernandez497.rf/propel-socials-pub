@extends('layouts.dashboard')

@section('title')
    Messages
@endsection

@section('content')
    @if ($messages->count()==0)
        <div class="card-header">
            <a href="{{ route('showAdminInbox') }}" class="btn btn-info">Inbox</a>
            <hr>
        </div>
        <div class="card-body">
            <div class="alert alert-info">
                There are no current messages.
            </div>
        </div>
    @else
        <div class="card-header">
            <h5 class="title">
                List of Messages
            </h5>
            <a href="{{ route('showAdminInbox') }}" class="btn btn-info">Inbox</a>
            <hr>
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <th>Sender Name</th>
                    <th>Email</th>
                    <th>Contact Number</th>
                    <th>Content</th>
                    {{-- <th>Action</th> --}}
                </thead>
                <tbody>
                    @foreach ($messages as $m)
                        <tr>
                            <td>{{ $m->name }}</td>
                            <td>{{ $m->email }}</td>
                            <td>{{ $m->contact_no }}</td>
                            <td>{{ substr($m->message, 0,20) . '...' }} <a href="{{ route('showMDetails', ['id' => $m->id ]) }}">Read more</a></td>
                            {{-- <td>
                                <a href="{{ route('showMDetails', ['id' => $m->id ]) }}" class="btn btn-primary">
                                    Details
                                </a>
                            </td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="container" style="margin-left:40%">
                {{ $messages->links() }}
            </div>
        </div>
    @endif
@endsection