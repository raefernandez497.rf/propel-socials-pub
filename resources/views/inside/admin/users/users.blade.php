@extends('layouts.dashboard')

@section('title')
    <h5 class="title">Users</h5> 
@endsection

@section('content')
    @if (session('deleted'))
        <script>
            swal({
                title: "SUCCESS",
                text: "User successfully deleted!",
                icon: "success",
                button: "Ok",
                });
        </script>
    @endif
    <div class="card-header">
        <h5 class="title">User List</h5>
        <hr>
        <a href="{{route('showAddUser')}}" class="btn btn-primary">
            New User
        </a>
        <hr>
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                {{-- <th>ID</th> --}}
                <th>User Type</th>
                <th>Name</th>
                <th>Email</th>
                {{-- <th>Action</th> --}}
                @if (auth()->user()->id==1)
                    <th>Action</th>
                @endif
            </thead>
            <tbody>
                @foreach ($users as $user)
                    {{-- @if ($user->id!=auth()->user()->id)
                        
                    @endif --}}
                    @if ($user->id!=1)
                        <tr>
                            {{-- <td>{{ $user->id }}</td> --}}
                            <td>{{ $user->user_types->name }}</td>
                            <td>{{ $user->lname . ', ' . $user->fname . ' ' . substr($user->mname, 0,1) }}</td>
                            <td>{{ $user->email }}</td>
                            @if (auth()->user()->id==1)
                                <td>
                                    <a href="{{ route('deleteUser', [ 'id' => $user->id ]) }}" class="btn btn-primary">Remove</a>
                                </td>
                            @endif
                            {{-- <td>
                                <a href="#details" class="btn btn-primary">
                                    Details
                                </a>
                            </td> --}}
                        </tr>
                    @endif
                @endforeach
                
            </tbody>
        </table>
        {{-- Pagination --}}
        <div class="container" style="margin-left:40%">
            {{ $users->links() }}
        </div>
    </div>
@endsection