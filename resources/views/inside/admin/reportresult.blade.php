<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Report</title>
</head>
<body>
    <div style="text-align:center!important;">
        <h1>PROPEL SOCIALS</h1>
        <h3 style="font-style:italic;">A Social Media Management Business</h3>
        <h4 style="margin-bottom:3%;">2F, Eduave Building, T.N. Pepito Street, Poblacion, Valencia City</h4>
        <hr>
        <h4 style="font-weight:bold;">WEEKLY PERFORMANCE REPORT AND ANALYSIS</h4>
        <h4> For the Week ended: <u>Insert Week end date here</u></h4>
    </div>
    <div>
        <h5>Business Client: <u>Client Name</u></h5>
        <h5>Business Address: <u>Client Address</u></h5>
        <h5>Subscription Package: <u>Client Subscription</u></h5>
        <h5>Subscription Period: <u>Period Date</u></h5>
        <hr>
        <h5><b>Facebook:</b>(Average for the !!Package details!!)</h5>
        <h5>Reach: <u>1</u></h5>
        <h5>Engagement: <u>2</u></h5>
        <h5>Shares: <u>3</u></h5>
        <h5>Average Ratings: <u>4</u></h5>
    </div>
</body>
</html>