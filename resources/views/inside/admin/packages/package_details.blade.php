@extends('layouts.dashboard')

@section('title')
    Packages
@endsection

@section('separate')
<div class="col-md-4">
    <div class="card">
        <div class="card-header">
            <h5 class="title">Package Details</h5>
        </div>
        <div class="card-body">
            <div>
                @foreach ($packages as $item)
                    <h5><b>Name: </b>{{ $item->packages->name }}</h5>
                    <h5><b>Price: </b>{{ $item->packages->price . '.00 PHP'}}</h5>
                    <h5><b>Posts per social media: </b>{{ $item->content_creation }}</h5>
                    <h5><b>Other Benefits: </b></h5>
                    <ul>
                    @if ($item->social_creation==1)
                        <li>Creation of social media accounts</li>
                    @endif
                    @if ($item->optimization==1)
                        <li>Business Page Optimization</li>
                    @endif
                    @if ($item->strategy==1)
                        <li>Social Meda Strategy</li>
                    @endif
                    @if ($item->followers_increase==1)
                        <li>Increase in followers</li>
                    @endif
                    @if ($item->account_mgt==1)
                        <li>Account management</li>
                    @endif
                    @if ($item->progress_report==1)
                        <li>Weekly/Monthly Progress Report</li>
                    @endif
                    @if ($item->no_setup_fee==1)
                        <li>No Setup Fee</li>
                    @endif
                    @if ($item->cancel_anytime==1)
                        <li>Cancel any time</li>
                    @endif
                    @if ($item->sm_analysis==1)
                        <li>Social media competitor analysis</li>
                    @endif
                    @if ($item->market_analysis==1)
                        <li>Market analysis</li>
                    @endif
                    @if ($item->spam_c_monitoring==1)
                        <li>Spam/comments monitoring</li>
                    @endif
                    @if ($item->ad_conv_analysis==1)
                        <li>Advertising conversion analysis</li>
                    @endif
                    </ul>
                @endforeach
            <a href="{{ url()->previous() }}" class="btn btn-info">Back</a>
            </div>
        </div>
    </div>
</div>
@endsection