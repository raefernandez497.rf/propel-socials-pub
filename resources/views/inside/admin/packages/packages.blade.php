@extends('layouts.dashboard')

@section('title')
    <h5 class="title">Packages</h5>
@endsection

@section('content')
    <div class="card-header">
        <h5 class="title">
            Package List
        </h5>
        {{-- <a href="#" class="btn btn-primary">
            New Package
        </a> --}}
        {{-- <hr> --}}
    </div>
    <div class="card-body">
        @if ($packages->count()!=0)
            <table class="table table-hover">
                <thead>
                    {{-- <th>ID</th> --}}
                    <th>Name</th>
                    <th>Price</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($packages as $package)
                        <tr>
                            {{-- <td>{{ $package->package_id }}</td> --}}
                            <td>{{ $package->name }}</td>
                            <td>{{ $package->price . '.00 PHP'}}</td>
                            <td>
                                <a href="{{ route('showPDetails', ['id' => $package->package_id]) }}" class="btn btn-primary" title="Show details">Details</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- Pagination --}}
            <div class="container" style="margin-left:40%">
                {{ $packages->links() }}
            </div>
        @else
            <div class="alert alert-warning" role="alert">
                There are no current packages!
            </div>
        @endif
    </div>
@endsection