@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <h4 align="center">User Information</h4>
                        <hr>
                        <div class="form-group row">
                            <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="fname" type="text" class="form-control{{ $errors->has('fname') ? ' is-invalid' : '' }}" name="fname" value="{{ old('fname') }}" required autofocus>

                                @if ($errors->has('fname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mname" class="col-md-4 col-form-label text-md-right">{{ __('Middle Name') }}</label>

                            <div class="col-md-6">
                                <input id="mname" type="text" class="form-control{{ $errors->has('mname') ? ' is-invalid' : '' }}" name="mname" value="{{ old('mname') }}" required autofocus>

                                @if ($errors->has('mname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="lname" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname" value="{{ old('lname') }}" required autofocus>

                                @if ($errors->has('lname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sex" class="col-md-4 col-form-label text-md-right">{{ __('Sex') }}</label>

                            <div class="col-md-6">
                                <select name="sex" id="sex" class="form-control">
                                    <option value="" disabled>Select Sex</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birthday" class="col-md-4 col-form-label text-md-right">{{ __('Birth Date') }}</label>

                            <div class="col-md-6">
                                <input id="birthday" type="date" class="form-control" name="birthday" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="contactno" class="col-md-4 col-form-label text-md-right">{{ __('Contact Number') }}</label>

                            <div class="col-md-6">
                                <input id="contactno" type="number" class="form-control" name="contactno" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <hr>
                        <h4 align="center">Business Information</h4>
                        <hr>

                        <div class="form-group row">
                            <label for="nusiness_name" class="col-md-4 col-form-label text-md-right">{{ __('Business Name') }}</label>

                            <div class="col-md-6">
                                <input id="business_name" type="text" class="form-control" name="business_name" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date_established" class="col-md-4 col-form-label text-md-right">{{ __('Date Established') }}</label>

                            <div class="col-md-6">
                                <input id="date_established" type="date" class="form-control" name="date_established" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="business_type" class="col-md-4 col-form-label text-md-right">{{ __('Business Type') }}</label>

                            <div class="col-md-6">
                                <select name="business_type" id="business_type" class="form-control">
                                    <option value="" disabled>Select Type</option>
                                    <option value="1">Service Business</option>
                                    <option value="2">Merchandising Business</option>
                                    <option value="3">Manufacturing</option>
                                    <option value="4">Others</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="business_address" class="col-md-4 col-form-label text-md-right">{{ __('Business Address') }}</label>

                            <div class="col-md-6">
                                {{-- <input id="business_name" type="text" class="form-control" name="business_name" required> --}}
                                <textarea name="business_address" id="business_address" cols="10" rows="3" class="form-control"></textarea>
                            </div>
                        </div>

                        <hr>
                        <h4 align="center">Social Media Information</h4>
                        <hr>
                        <p class="text-center"><b>Note:</b>&nbsp;Please enter the <span style="color:red;">social media accounts of your business.<span></p>
                            <p class="text-center"><i class="fab fa-3x fa-facebook"></i></p>
                        <div class="form-group row">
                            <label for="fb_name" class="col-md-4 col-form-label text-md-right">{{ __('Facebook Page Name') }}</label>

                            <div class="col-md-6">
                                <input id="fb_name" type="text" class="form-control" name="fb_name" placeholder="Facebook Page Name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fb_url" class="col-md-4 col-form-label text-md-right">{{ __('Facebook Page URL') }}</label>

                            <div class="col-md-6">
                                <input id="fb_url" type="text" class="form-control" name="fb_url" placeholder="e.g. www.facebook.com/example" value="www.facebook.com/">
                            </div>
                        </div>
                        <br>
                            <p class="text-center"><i class="fab fa-3x fa-twitter"></i></p>
                        <div class="form-group row">
                            <label for="twitter_name" class="col-md-4 col-form-label text-md-right">{{ __('Twitter Profile Name') }}</label>

                            <div class="col-md-6">
                                <input id="twitter_name" type="text" class="form-control" name="twitter_name" placeholder="Business Twitter Profile">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="twitter_url" class="col-md-4 col-form-label text-md-right">{{ __('Twitter Profile URL') }}</label>

                            <div class="col-md-6">
                                <input id="twitter_url" type="text" class="form-control" name="twitter_url" placeholder="e.g. www.twitter.com/example" value="www.twitter.com/">
                            </div>
                        </div>
                        <br>
                            <p class="text-center"><i class="fab fa-3x fa-instagram"></i></p>
                        <div class="form-group row">
                            <label for="inst_name" class="col-md-4 col-form-label text-md-right">{{ __('Instagram Username') }}</label>

                            <div class="col-md-6">
                                <input id="inst_name" type="text" class="form-control" name="inst_name" placeholder="Username">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inst_url" class="col-md-4 col-form-label text-md-right">{{ __('Instagram Profile URL') }}</label>

                            <div class="col-md-6">
                                <input id="inst_url" type="text" class="form-control" name="inst_url" placeholder="e.g. instagram.com/example" value="www.instagram.com/">
                            </div>
                        </div>

                        <hr>
                        <h4 align="center">Choose Plan</h4>
                            <div class="form-group row" style="padding:0%3%0%3%;">
                                <table class="table table-hover">
                                    <thead>
                                        <th></th>
                                        <th>Gold</th>
                                        <th>Platinum</th>
                                        <th>Diamond</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <input class="form-control" type="radio" name="plan" value="1">
                                            </td>
                                            <td>
                                                <input class="form-control" type="radio" name="plan" value="2">
                                            </td>
                                            <td>
                                                <input class="form-control" type="radio" name="plan" value="3">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        <hr>
                        <h4 align="center">Card Information</h4>
                        <hr>

                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Card Number') }}</label>

                            <div class="form-inline col-md-6">
                                <input id="card_info_1" type="number" class="form-control col-md-3" name="card_info_1" size="4" required>
                                &nbsp;-&nbsp;
                                <input id="card_info_2" type="number" class="form-control col-md-3" name="card_info_2" size="4" required>
                                &nbsp;-&nbsp;
                                <input id="card_info_3" type="number" class="form-control col-md-3" name="card_info_3" size="4" required>
                            </div>
                        </div>
                        <div class="form-group" align="center">
                            {{-- 6LeKUYIUAAAAAAOIg7pU5ynOi3L7N8VaG7c8jdbQ --}}
                            {{-- <div class="g-recaptcha" data-sitekey="6LdRL4EUAAAAAIR93jPTH-eYLec4RUbVUu-wPZY_"></div> --}}
                            <div class="g-recaptcha" data-sitekey="6LeKUYIUAAAAAAOIg7pU5ynOi3L7N8VaG7c8jdbQ"></div>
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
